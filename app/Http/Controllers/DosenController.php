<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\DosenImport;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\PeminatanController;
use DB;

use App\Dosen;
use App\Peminatan;

class DosenController extends Controller
{
  public function index()
  {
    $data['dosen'] = Dosen::all();
    $data['peminatan'] = Peminatan::all();

    if ($data['peminatan'] == null) {
      return redirect('/master/data-peminatan');
    }

    return view('admin.dosen.data-dosen', $data);
  }

  /**
  * @return view form tambah peminatan
  */
  public function tambah()
  {
    $peminatan = Peminatan::all();
    return view('admin.dosen.tambah', ['peminatan' => $peminatan]);
  }

  public function proses_tambah(Request $request)
  {
    $this->validate($request, [
      'nama' => 'required|string|max:255',
      'peminatan' => 'required',
    ]);

    Dosen::insert([
      'nama' => $request->nama,
      'id_peminatan' => $request->peminatan,
    ]);

    return redirect('/master/data-dosen');
  }

  public function edit($id)
  {
    $data['dosen'] = Dosen::find($id);
    $data['peminatan'] = Peminatan::all();
    return view('admin.dosen.edit', $data);
  }

  public function proses_edit(Request $request,$id)
  {
    $this->validate($request, [
      'nama' => 'required|string|max:255',
      'peminatan' => 'required',
    ]);

    Dosen::where('id', $id)
    ->update([
      'nama' => $request->nama,
      'id_peminatan' => $request->peminatan,
    ]);

    return redirect('/master/data-dosen');
  }

  public function hapus($id)
  {
    DB::table('data_dosen')
    ->where('id', $id)
    ->delete();

    return redirect('/master/data-dosen');
  }

  public function truncate()
  {
    DB::table('data_dosen')
    ->delete();
    return redirect('/master/data-dosen');
  }

  public function import(Request $request)
  {
    $this->validate($request, [
      'file' => 'required|mimes:csv,xls,xlsx'
    ]);

    $file = $request->file('file');
    $nama_file = rand().$file->getClientOriginalName();
    $file->move('dosen',$nama_file);

    // import data
    Excel::import(new DosenImport, public_path('/dosen/'.$nama_file));

    return redirect('/master/data-dosen');

  }
}
