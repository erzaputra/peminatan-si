<?php

namespace App\Http\Controllers;

use App\ServerModel;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Alert;
use Auth;

class ServerController extends Controller
{

    /**
     * mengubah status sesuai dengan id.
     *
     * @return \Illuminate\Http\Response
     */
    public function status($id)
    {
        $server = ServerModel::find($id);
        if ($server->status == 0) {
          ServerModel::where('id', $id)
          ->update([
            'status' => 1,
            'updated_by' => Auth::user()->name,
            'updated_at' => Carbon::now(),
          ]);
        }else{
          ServerModel::where('id', $id)
          ->update([
            'status' => 0,
            'updated_by' => Auth::user()->name,
            'updated_at' => Carbon::now(),
          ]);
        }

        Alert::success('Berhasil', 'Berhasil mengubah '.$server->nama_status);
        return redirect('/home');
    }

    public function generate($id)
    {
      $server = ServerModel::find($id);
      if ($server->nama_status == "Status Kuota") {
        $peminatan = new PeminatanController();
        if ($server->status == 0) {
          $hasil = $peminatan->kuota();
          if ($hasil == false) {
            Alert::error('Gagal', 'Pastikan data sudah dosen atau mahasiswa sudah terisi');
            return redirect()->back();
          }
        }else{
          $peminatan->resetKuota();
        }
      }else{
        $seleksi = new DataPeminatanController();
        if ($server->status == 0) {
          $seleksi->seleksi();
        }else{
          $seleksi->resetSeleksi();
        }
      }

      $this->status($id);
      Alert::success('Berhasil', 'Berhasil mengubah '.$server->nama_status);
      return redirect('/home');
    }
}
