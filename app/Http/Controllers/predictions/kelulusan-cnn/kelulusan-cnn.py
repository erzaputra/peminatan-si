#!/usr/bin/env python
# coding: utf-8

# In[1]:

import os

import pandas as pd
import numpy as np
import matplotlib.pylab as plt

import tensorflow as tf
# from tensorflow import keras
from keras.models import Sequential, Model
from keras.layers import Input, Dense
from keras.utils import to_categorical

import sklearn
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt

dir_path = os.path.dirname(os.path.realpath(__file__))
df = pd.read_csv(dir_path+'/datamahasiswa2015.csv')
# In[2]:

new_class = [0] * len(df['Keterangan'])
for index, row in df.iterrows() :
    if row['Keterangan'] == "TEPAT WAKTU" :
        new_class[index] = 1
    else :
        new_class[index] = 0


# In[3]:


df["ket_status"] = new_class


# In[4]:


mainData = df.drop(['Status','Keterangan','Pilihan 1','Peminatan','Tanggal Lulus','No','NIM','MP1','MP2','MP3','RPL'],axis=1)
mainData.rename(columns={'KSI':'Manprosi'}, inplace=True)


# In[5]:


target = ["ket_status"]
predictors = list(set(list(mainData.columns))-set(target))
mainData[predictors] = mainData[predictors]/mainData[predictors].max()


# In[6]:


X = mainData[target]
y = mainData[predictors]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=40)

# In[7]:


# y_train = to_categorical(y_train)
# y_test = to_categorical(y_test)

X_trains = to_categorical(X_train)
X_tests = to_categorical(X_test)


# In[8]:


model = Sequential()
model.add(Dense(128, activation='relu', input_dim=19))
model.add(Dense(64, activation='relu'))
model.add(Dense(32, activation='relu'))
model.add(Dense(2, activation='softmax'))

# Compile the model
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])


# In[9]:


model.fit(y_train, X_trains, epochs=200)


# In[10]:


pred_train= model.predict(y_train)
scores = model.evaluate(y_train, X_trains, verbose=0)

pred_test= model.predict(y_test)
scores2 = model.evaluate(y_test, X_tests, verbose=0)


# In[11]:


hasil = pd.DataFrame()
hasil["NIM"] = df["NIM"]
hasil["ket_status"] = df["ket_status"]
temp = model.predict_classes(y_test)

# hasil = model.predict_classes(y_test)


# In[12]:


pre = [0] * len(hasil)
for index, row in hasil.iterrows():
    if (index in y_test.index) :
        pre[index] = 1
    else :
        pre[index] = "train data"

hasil["forecast"] = pre


# In[13]:


dataTesting = pd.read_excel(dir_path+'/Nilai Master Pipe.xlsx')


# In[14]:


dataTestingFilter = dataTesting[predictors]
dataTestingHasil = model.predict_classes(dataTestingFilter)

dataTesting["Lulus Tepat Waktu"] = dataTestingHasil
dataTesting.rename(columns={'nim':'NIM'}, inplace=True)
dataTesting.drop(columns="nama",axis=1).head()

dataTesting.to_excel(dir_path+'/Hasil CNN.xlsx')

# In[18]:
