<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Alert;
use App\User;

use App\DataNilai;
use App\Dosen;
use App\Peminatan;
use App\DataPeminatan;
use App\Keprof;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index()
  {

    $data['server'] = DB::table('server')->get();
    $data['peminatan'] = Peminatan::all();
    $data['mahasiswa'] = DataNilai::all();
    $data['dosen'] = Dosen::all();

    if (Auth::user()->id_role == 1) {
      return view('admin.dashboard', $data);
    }else if(Auth::user()->id_role == 2){
      if ($data['server'][0]->status == 0) {
        Auth::logout();
        Alert::error('Fail', 'Login disabled');
        return redirect('/home');
      }
      $data['pilihan_peminatan'] = DataPeminatan::with('users','peminatan_1','peminatan_2')
      ->where('id_users', Auth::user()->id)
      ->get();
      return view('mahasiswa.home', $data);
    }else{
      return redirect('/prioritas');
    }
  }

  public function pengumuman()
  {

    $server = DB::table('server')->get();
    if ($server[4]->status == 0) {
      Alert::error('Gagal', 'Pengumuman belum dibuka, silahkan bersabar dan berdoa');
      return redirect('/home');
    }

    $peminatan = DataPeminatan::with('peminatans')
    ->where('id_users', Auth::user()->id)
    ->first();

    if($peminatan == null)
    {
        Alert::error('Gagal', 'Anda belum mendaftar peminatan');
        return redirect('/home');
    }

    return view('mahasiswa.pengumuman', ['peminatan' => $peminatan->peminatans]);

  }

    public function tidakdaftar(){
        
        $hasil['hasil'] = DB::table('data_nilai_mahasiswa')
        ->select('data_nilai_mahasiswa.*')
        ->leftJoin('users', 'users.nim', '=', 'data_nilai_mahasiswa.nim')
        ->leftJoin('data_peminatan', 'users.id', '=', 'data_peminatan.id_users')
        ->where('data_peminatan.id', null)
        ->get();
        
        $hasil;
        $i=0;
        foreach($hasil['hasil'] as $nilai){
            //techno
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $nilai->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == 1) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->oop+$nilai->rpb+$nilai->web+$nilai->pi;
            $total = $skorKeprof + $skor;
            $hasil['techno'][$i] = $total;
            
            //EDM
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $nilai->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == 2) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->statistik+$nilai->matdis+$nilai->alpro+$nilai->strukdat;
            $total = $skorKeprof + $skor;
            $hasil['EDM'][$i] = $total;
            
            //ESD
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $nilai->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == 3) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->oop+$nilai->apsi+$nilai->rpb+$nilai->web;
            $total = $skorKeprof + $skor;
            $hasil['ESD'][$i] = $total;
            
            //ERP
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $nilai->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == 4) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->se+$nilai->po+$nilai->rpb+$nilai->scm;
            $total = $skorKeprof + $skor;
            $hasil['ERP'][$i] = $total;
            
            //EA
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $nilai->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == 5) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->ea+$nilai->apsi+$nilai->rpb+$nilai->basdat;
            $total = $skorKeprof + $skor;
            $hasil['EA'][$i] = $total;
            
            //ISM
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $nilai->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == 6) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->manjarkom+$nilai->se+$nilai->sisop+$nilai->msdm;
            $total = $skorKeprof + $skor;
            $hasil['ISM'][$i] = $total;
            
            //EIM
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $nilai->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == 7) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->desjar+$nilai->manjarkom+$nilai->sisop+$nilai->manprosi;
            $total = $skorKeprof + $skor;
            $hasil['EIM'][$i] = $total;
            
            $i++;
        }
        
        
        return view('tidakdaftar', $hasil);
        
    }

  public function cek()
  {
    $students = DataNilai::all();
    return $students[0];
  }
}
