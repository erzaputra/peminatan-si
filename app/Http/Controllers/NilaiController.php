<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\NilaiImport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

use App\DataNilai;

class NilaiController extends Controller
{

  public function index()
  {
    $nilai = DataNilai::all();
    return view('admin.nilai.data-nilai-mahasiswa', ['nilai' => $nilai]);
  }

  /**
  * @return view form tambah peminatan
  */
  public function tambah()
  {
    return view('admin.nilai.tambah');
  }

  /**
  * menambahkan data yang telah diinput di view tambah
  *
  * @return redirect /mastar/data-nilai-mahasiswa
  */
  public function proses_tambah(Request $request)
  {
    $this->validate($request, [
      'nim' => 'required|numeric',
      'nama' => 'required|string|max:255',
      'oop' => 'required',
      'rpb' => 'required',
      'pi' => 'required',
      'apsi' => 'required',
      'web' => 'required',
      'statistik' => 'required',
      'matdis' => 'required',
      'alpro' => 'required',
      'strukdat' => 'required',
      'se' => 'required',
      'po' => 'required',
      'scm' => 'required',
      'ea' => 'required',
      'basdat' => 'required',
      'manjarkom' => 'required',
      'sisop' => 'required',
      'msdm' => 'required',
      'desjar' => 'required',
      'manprosi' => 'required',
    ]);

    DataNilai::insert([
      'nim' => $request->nim,
      'nama' => $request->nama,
      'oop' => $request->oop,
      'rpb' => $request->rpb,
      'pi' => $request->pi,
      'apsi' => $request->apsi,
      'web' => $request->web,
      'statistik' => $request->statistik,
      'matdis' => $request->matdis,
      'alpro' => $request->alpro,
      'strukdat' => $request->strukdat,
      'se' => $request->se,
      'po' => $request->po,
      'scm' => $request->scm,
      'ea' => $request->ea,
      'basdat' => $request->basdat,
      'manjarkom' => $request->manjarkom,
      'sisop' => $request->sisop,
      'msdm' => $request->msdm,
      'desjar' => $request->desjar,
      'manprosi' => $request->manprosi,
    ]);

    return redirect('/master/data-nilai-mahasiswa');
  }

  /**
  * menghapus data sesuai dengan id
  *
  * @return redirect /mastar/data-nilai-mahasiswa
  */
  public function hapus(Request $request, $id){
    DB::table('data_nilai_mahasiswa')
    ->where('id', $id)
    ->delete();

    return redirect('/master/data-nilai-mahasiswa');
  }

  /**
  * menampilkan data yang akan diedit sesuai id
  *
  * @return data peminatna sesuai id
  */
  public function edit($id){
    $data = DataNilai::find($id);
    return view('admin.nilai.edit', ['nilai' => $data]);
  }

  /**
  * mengedit data yang telah diinput di view edit
  *
  * @return redirect /mastar/data-nilai-mahasiswa
  */
  public function proses_edit(Request $request, $id)
  {
    $this->validate($request, [
      'nim' => 'required|numeric',
      'nama' => 'required|string|max:255',
      'oop' => 'required',
      'rpb' => 'required',
      'pi' => 'required',
      'apsi' => 'required',
      'web' => 'required',
      'statistik' => 'required',
      'matdis' => 'required',
      'alpro' => 'required',
      'strukdat' => 'required',
      'se' => 'required',
      'po' => 'required',
      'scm' => 'required',
      'ea' => 'required',
      'basdat' => 'required',
      'manjarkom' => 'required',
      'sisop' => 'required',
      'msdm' => 'required',
      'desjar' => 'required',
      'manprosi' => 'required',
    ]);

    DataNilai::where('id', $id)
    ->update([
      'nim' => $request->nim,
      'nama' => $request->nama,
      'oop' => $request->oop,
      'rpb' => $request->rpb,
      'pi' => $request->pi,
      'apsi' => $request->apsi,
      'web' => $request->web,
      'statistik' => $request->statistik,
      'matdis' => $request->matdis,
      'alpro' => $request->alpro,
      'strukdat' => $request->strukdat,
      'se' => $request->se,
      'po' => $request->po,
      'scm' => $request->scm,
      'ea' => $request->ea,
      'basdat' => $request->basdat,
      'manjarkom' => $request->manjarkom,
      'sisop' => $request->sisop,
      'msdm' => $request->msdm,
      'desjar' => $request->desjar,
      'manprosi' => $request->manprosi,
    ]);

    return redirect('/master/data-nilai-mahasiswa');
  }

  /**
  * menghapus seluruh data nilai mahasiswa
  *
  * @return redirect /mastar/data-nilai-mahasiswa
  */
  public function truncate()
  {
    DB::table('data_nilai_mahasiswa')->delete();
    return redirect('/master/data-nilai-mahasiswa');
  }


  /**
  * mengimport data nilai mahasiswa
  *
  * @return redirect /mastar/data-nilai-mahasiswa
  */
  public function import(Request $request){
    $this->validate($request, [
      'file' => 'required|mimes:csv,xls,xlsx'
    ]);

    $file = $request->file('file');
    $nama_file = rand().$file->getClientOriginalName();
    $file->move('nilai',$nama_file);

    // import data
    Excel::import(new NilaiImport, public_path('/nilai/'.$nama_file));

    //generate kuota
    $kuota = new PeminatanController;
    $kuota->kuota();

    return redirect('/master/data-nilai-mahasiswa');

  }
}
