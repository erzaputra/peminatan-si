<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataPeminatan;
use App\Exports\NilaiExport;
use App\Imports\KelulusanCnnImport;
use Maatwebsite\Excel\Facades\Excel;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use Alert;
class PredictionController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function kelulusan_cnn_index()
  {
    $hasil = DataPeminatan::all();
    return view('admin.predictions.kelulusan-cnn', compact('hasil'));
  }

  public function kelulusan_cnn_process(Request $request)
  {

    Excel::store(new NilaiExport, 'Nilai Master Pipe.xlsx', 'app_path');

    try{
    //   $process = new Process(['python', app_path().'/Http/Controllers/predictions/kelulusan-cnn/cgi-bin/kelulusan-cnn.py']);
    //   $process->run();
      $result = shell_exec("python ".app_path().'/Http/Controllers/predictions/kelulusan-cnn/kelulusan-cnn.py');
    }catch(Exception $e){

      return "error";
    }

    Excel::import(new KelulusanCnnImport, app_path().'/Http/Controllers/predictions/kelulusan-cnn/Hasil CNN.xlsx');
    Alert::success('Berhasil', 'Berhasil melakukan prediksi CNN');
    return redirect()->back();
  }

  public function kelulusan_cnn_reset(Request $request)
  {
    DataPeminatan::where('status_lulus','!=',null)
    ->update([
      'status_lulus' => null,
    ]);
    Alert::success('Berhasil', 'Berhasil di Reset!');
    return redirect()->back();
  }
}
