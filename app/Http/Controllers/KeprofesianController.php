<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\KeprofImport;
use Maatwebsite\Excel\Facades\Excel;

use App\Keprof;
use App\Peminatan;
use App\DataNilai;

use DB;

class KeprofesianController extends Controller
{
  public function index(){
    $data['keprof'] = Keprof::all();
    $data['peminatan'] = Peminatan::all();
    return view('admin.keprof.data-mahasiswa-keprof', $data);
  }

  public function tambah()
  {
    $data['peminatan'] = Peminatan::all();
    $data['nilai'] = DataNilai::all();
    return view('admin.keprof.tambah', $data);
  }

  public function proses_tambah(Request $request)
  {
    $this->validate($request, [
      'nim' => 'required',
      'keprof' => 'required',
    ]);

    Keprof::insert([
      'nim' => $request->nim,
      'id_keprof' => $request->keprof,
    ]);

    return redirect('/master/data-mahasiswa-keprof');
  }

  public function edit($id)
  {
    $data['mahasiswa'] = Keprof::with('data_nilai_mahasiswa')
    ->where('id', $id)
    ->get();
    $data['peminatan'] = Peminatan::all();
    return view('admin.keprof.edit', $data);
  }

  public function proses_edit(Request $request,$id)
  {
    $this->validate($request, [
      'nim' => 'required',
      'keprof' => 'required',
    ]);

    Keprof::where('id', $id)
    ->update([
      'nim' => $request->nim,
      'id_keprof' => $request->keprof,
    ]);

    return redirect('/master/data-mahasiswa-keprof');
  }

  public function hapus($id)
  {
    DB::table('data_keprof')
    ->where('id', $id)
    ->delete();

    return redirect('/master/data-mahasiswa-keprof');
  }

  public function truncate()
  {
    DB::table('data_keprof')
    ->delete();
    return redirect('/master/data-mahasiswa-keprof');
  }


  public function import(Request $request)
  {
    $this->validate($request, [
      'file' => 'required|mimes:csv,xls,xlsx'
    ]);

    $file = $request->file('file');
    $nama_file = rand().$file->getClientOriginalName();
    $file->move('keprof',$nama_file);

    // import data
    Excel::import(new KeprofImport, public_path('/keprof/'.$nama_file));

    return redirect('/master/data-mahasiswa-keprof');

  }
}
