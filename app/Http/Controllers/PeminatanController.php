<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminatan;
use App\Dosen;
use App\DataNilai;
use DB;
use Alert;
use App\DataPeminatan;

class PeminatanController extends Controller
{
  /**
  * menampilkan tabel data peminatan
  *
  * @return seluruh data peminatan
  */
  public function index()
  {
    $data = Peminatan::all();
    return view('admin.peminatan.peminatan', ['peminatan' => $data]);
  }

  /**
  * @return view form tambah peminatan
  */
  public function tambah()
  {
    return view('admin.peminatan.tambah');
  }

  /**
  * menambahkan data yang telah diinput di view tambah
  *
  * @return redirect /mastar/data-peminatan
  */
  public function proses_tambah(Request $request)
  {
    $this->validate($request, [
      'nama' => 'required|string|max:255',
      'kelompok' => 'required',
      'keprof' => 'required',
      'singkatan' => 'required',
    ]);

    DB::transaction(function() use($request){
      Peminatan::insert([
        'name' => $request->nama,
        'kelompok' => $request->kelompok,
        'singkatan' => $request->singkatan,
        'keprof' => $request->keprof,
      ]);
      $this->resetKuota();
    });

    return redirect('/master/data-peminatan');
  }

  /**
  * menghapus data sesuai dengan id
  *
  * @return redirect /mastar/data-peminatan
  */
  public function hapus(Request $request, $id){
    DB::transaction(function() use($request,$id){
      DB::table('peminatan')
      ->where('id', $id)
      ->delete();
      $this->resetKuota();
    });

    return redirect('/master/data-peminatan');
  }

  /**
  * menampilkan data yang akan diedit sesuai id
  *
  * @return data peminatna sesuai id
  */
  public function edit($id){
    $data = Peminatan::find($id);
    return view('admin.peminatan.edit', ['peminatan' => $data]);
  }

  /**
  * mengedit data yang telah diinput di view edit
  *
  * @return redirect /mastar/data-peminatan
  */
  public function proses_edit(Request $request, $id)
  {
    $this->validate($request, [
      'nama' => 'required|string|max:255',
      'kelompok' => 'required',
      'keprof' => 'required',
      'singkatan' => 'required',
    ]);

    DB::transaction(function() use($request, $id){
      Peminatan::where('id', $id)
      ->update([
        'name' => $request->nama,
        'kelompok' => $request->kelompok,
        'singkatan' => $request->singkatan,
        'keprof' => $request->keprof,
      ]);
      $this->resetKuota();
    });

    return redirect('/master/data-peminatan');
  }

  /**
  * menghitung kuota dengan algoritma
  * membutuhkan data dosen, nilai, dan peminatan
  *
  * @return none
  */
  public function kuota(){
    //deklarasi parameter untuk menentukan kuota
    $jumlahDosen = Dosen::all();
    $jumlahMahasiswa = DataPeminatan::count();
    $peminatans = Peminatan::all();
    $kuotaPerdosen = 0;

    if ($jumlahDosen->count() != 0 && $jumlahMahasiswa != 0) {
      $kuotaPerdosen = $jumlahMahasiswa / $jumlahDosen->count();

      //insert kuota sesuai kuota peminatan
      foreach ($peminatans as $peminatan) {

        $dosen = $jumlahDosen
        ->where('id_peminatan', $peminatan->id)
        ->count();

        Peminatan::where('id', $peminatan->id)
        ->update([
          'kuota' => ceil($dosen*$kuotaPerdosen),
          'sisa_kuota' => ceil($dosen*$kuotaPerdosen),
        ]);
      }
      return true;

    }else{
      //netral menjadi 0
      $this->resetKuota();
      return false;
    }
  }

  /**
  * mereset kuota seluruh peminatan
  *
  * @return updated kuota peminatan
  */
  public function resetKuota(){
    return DB::table('peminatan')->update([
      'kuota' => 0,
      'sisa_kuota' => 0,
    ]);
  }

}
