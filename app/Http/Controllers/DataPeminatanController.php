<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Exports\HasilExport;
use Maatwebsite\Excel\Facades\Excel;
use App\DataNilai;
use App\DataPeminatan;
use App\Peminatan;
use App\Keprof;
use App\ServerModel as Server;
use Carbon\Carbon;
use Alert;
use DB;

class DataPeminatanController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function insert(Request $request)
  {

    $data = Server::all();
    if ($data[1]->status == 0) {
      Alert::error('Gagal', 'Pendaftaran disabled');
      return redirect('/home');
    }

    $this->validate($request,[
      'pilihan_1' => 'required',
      'pilihan_2' => 'required',
    ]);

    $pilihan1 = $request->pilihan_1;
    $peminatan1 = Peminatan::find($pilihan1);
    $pilihan2 = $request->pilihan_2;
    $peminatan2 = Peminatan::find($pilihan2);
    if ($peminatan1->kelompok == $peminatan2->kelompok) {
      Alert::error('Gagal', 'Tidak bisa memilih kelompok peminatan yang sama :)');
      return redirect('/home');
    }

    $data = DataPeminatan::where('id_users', Auth::user()->id)->get();
    if ($data != "[]") {
      Alert::error('Gagal', 'Anda hanya boleh melakukan seleksi 1x saja');
      return redirect('/home');
    }

    $peminatan = Peminatan::all();
    $nilai = DataNilai::where('nim', Auth::user()->nim)->first();
    $pilihan = [$request->pilihan_1,$request->pilihan_2];
    for ($i=0; $i < count($pilihan); $i++) {
      if ($pilihan[$i] == "1") {
        $keprof = DataNilai::with('data_keprof')
        ->where('nim', Auth::user()->nim)
        ->first();
        $skorKeprof = 0;
        if ($keprof->data_keprof != null) {
          if ($keprof->data_keprof->id_keprof == $pilihan[$i]) {
            $skorKeprof = 2;
          }
        }
        $skor = $nilai->oop+$nilai->rpb+$nilai->web+$nilai->pi;
        $total = $skorKeprof + $skor;
        if ($i == 0) {
          $total++;
          $skor_peminatan_1 = $total;
        }else{
          $skor_peminatan_2 = $total;
        }
      }else if($pilihan[$i] == "2") {
        $keprof = DataNilai::with('data_keprof')
        ->where('nim', Auth::user()->nim)
        ->first();
        $skorKeprof = 0;
        if ($keprof->data_keprof != null) {
          if ($keprof->data_keprof->id_keprof == $pilihan[$i]) {
            $skorKeprof = 2;
          }
        }
        $skor = $nilai->statistik+$nilai->matdis+$nilai->alpro+$nilai->strukdat;
        $total = $skorKeprof + $skor;
        if ($i == 0) {
          $total++;
          $skor_peminatan_1 = $total;
        }else{
          $skor_peminatan_2 = $total;
        }
      }else if($pilihan[$i] == "3") {
        $keprof = DataNilai::with('data_keprof')
        ->where('nim', Auth::user()->nim)
        ->first();
        $skorKeprof = 0;
        if ($keprof->data_keprof != null) {
          if ($keprof->data_keprof->id_keprof == $pilihan[$i]) {
            $skorKeprof = 2;
          }
        }
        $skor = $nilai->oop+$nilai->apsi+$nilai->rpb+$nilai->web;
        $total = $skorKeprof + $skor;
        if ($i == 0) {
          $total++;
          $skor_peminatan_1 = $total;
        }else{
          $skor_peminatan_2 = $total;
        }
      }else if($pilihan[$i] == "4") {
        $keprof = DataNilai::with('data_keprof')
        ->where('nim', Auth::user()->nim)
        ->first();
        $skorKeprof = 0;
        if ($keprof->data_keprof != null) {
          if ($keprof->data_keprof->id_keprof == $pilihan[$i]) {
            $skorKeprof = 2;
          }
        }
        $skor = $nilai->se+$nilai->po+$nilai->rpb+$nilai->scm;
        $total = $skorKeprof + $skor;
        if ($i == 0) {
          $total++;
          $skor_peminatan_1 = $total;
        }else{
          $skor_peminatan_2 = $total;
        }
      }else if($pilihan[$i] == "5") {
        $keprof = DataNilai::with('data_keprof')
        ->where('nim', Auth::user()->nim)
        ->first();
        $skorKeprof = 0;
       if ($keprof->data_keprof != null) {
          if ($keprof->data_keprof->id_keprof == $pilihan[$i]) {
            $skorKeprof = 2;
          }
        }
        $skor = $nilai->ea+$nilai->apsi+$nilai->rpb+$nilai->basdat;
        $total = $skorKeprof + $skor;
        if ($i == 0) {
          $total++;
          $skor_peminatan_1 = $total;
        }else{
          $skor_peminatan_2 = $total;
        }
      }else if($pilihan[$i] == "6") {
        $keprof = DataNilai::with('data_keprof')
        ->where('nim', Auth::user()->nim)
        ->first();
        $skorKeprof = 0;
        if ($keprof->data_keprof != null) {
          if ($keprof->data_keprof->id_keprof == $pilihan[$i]) {
            $skorKeprof = 2;
          }
        }
        $skor = $nilai->manjarkom+$nilai->se+$nilai->sisop+$nilai->msdm;
        $total = $skorKeprof + $skor;
        if ($i == 0) {
          $total++;
          $skor_peminatan_1 = $total;
        }else{
          $skor_peminatan_2 = $total;
        }
      }else{
        $keprof = DataNilai::with('data_keprof')
        ->where('nim', Auth::user()->nim)
        ->first();
        $skorKeprof = 0;
        if ($keprof->data_keprof != null) {
          if ($keprof->data_keprof->id_keprof == $pilihan[$i]) {
            $skorKeprof = 2;
          }
        }
        $skor = $nilai->desjar+$nilai->manjarkom+$nilai->sisop+$nilai->manprosi;
        $total = $skorKeprof + $skor;
        if ($i == 0) {
          $total++;
          $skor_peminatan_1 = $total;
        }else{
          $skor_peminatan_2 = $total;
        }
      }
    }
    DataPeminatan::insert([
      'id_users' => Auth::user()->id,
      'id_peminatan_1' => $request->pilihan_1,
      'skor_peminatan_1' => $skor_peminatan_1,
      'id_peminatan_2' => $request->pilihan_2,
      'skor_peminatan_2' => $skor_peminatan_2,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);

    Alert::success('Berhasil', 'Berhasil memilih peminatan');
    return redirect('/home');
  }

  public function delete($id)
  {
    $data = Server::all();
    if ($data[1]->status == 0) {
      Alert::error('Gagal', 'Pendaftaran ditutup');
      return redirect('/home');
    }

    DataPeminatan::where('id', $id)->delete();
    Alert::success('Berhasil', 'Berhasil menghapus pilihan');
    return redirect('/home');
  }

  public function seleksi()
  {
    $this->resetSeleksi();

    //fase 1 - memasukan pilihan pertama ke peminatan
    $peminatan = Peminatan::all();
    foreach($peminatan as $pem)
    {
      $mahasiswa = DataPeminatan::where('id_peminatan_1', $pem->id)
      ->where('prioritas', 0)
      ->orderBy('skor_peminatan_1', 'DESC')
      ->get();

      $sisa_kuota = $pem->kuota;
      foreach($mahasiswa as $mhs)
      {
        if ($sisa_kuota != 0) {
          DataPeminatan::where('id', $mhs->id)
          ->update([
            'peminatan' => $pem->id,
            'status' => 1,
          ]);
          $sisa_kuota--;
        }
      }
      Peminatan::where('id', $pem->id)
      ->update([
        'sisa_kuota' => $sisa_kuota,
      ]);
    }
    //end fase 1

    //fase 2 - membandingkan pilihan ke 2 yang tidak diterima
      $peminatan = Peminatan::all();
      foreach($peminatan as $pem)
      {
        $mahasiswa = DataPeminatan::
        select(
          'data_peminatan.*',
          DB::raw('(CASE WHEN id_peminatan_1 = '.$pem->id.' THEN skor_peminatan_1 ELSE skor_peminatan_2 END) AS skor')
        )
        ->where(function($query) use($pem){
          $query->where('id_peminatan_1', $pem->id)
          ->where('status', 1);
        })
        ->orWhere(function($query) use($pem){
          $query->where('id_peminatan_2', $pem->id)
          ->where('status', 0);
        })
        ->where('prioritas', 0)
        ->orderBy('skor', 'DESC')
        ->get();

        $kuota = $pem->kuota;
        $terpakai = 0;
        for ($j=0; $j < count($mahasiswa); $j++) {
          if ($kuota >= $j+1) {
            DataPeminatan::where('id', $mahasiswa[$j]->id)
            ->update([
              'peminatan' => $pem->id,
              'status' => DB::raw('status+1'),
            ]);
            $terpakai++;
          }else{
            DataPeminatan::where('id', $mahasiswa[$j]->id)
            ->update([
              'peminatan' => null,
            ]);
          }
        }

        Peminatan::where('id', $pem->id)
        ->update([
          'sisa_kuota' => $kuota-$terpakai,
        ]);
      }
      //end fase 2

      //fase 3 -- looping untuk memfilter peminatan
      for ($i=2; $i < 10; $i++) {

      $peminatan = Peminatan::all();
      foreach($peminatan as $pem)
      {
        $mahasiswa = DataPeminatan::
        select(
          'data_peminatan.*',
          DB::raw('(CASE WHEN id_peminatan_1 = '.$pem->id.' THEN skor_peminatan_1 ELSE skor_peminatan_2 END) AS skor')
        )
        ->where(function($query) use($pem){
          $query->where('id_peminatan_2', $pem->id)
          ->orWhere('id_peminatan_1', $pem->id);
        })
        ->where(function($query) use($pem){
          $query->where('peminatan', $pem->id)
          ->orWhere('peminatan', null);
        })
        ->where(function($query) use($pem, $i){
          $query->where('status', $i)
          ->orWhere('status', $i-1);
        })
        ->where('prioritas', 0)
        ->orderBy('skor', 'DESC')
        ->get();

        $kuota = $pem->kuota;
        $terpakai = 0;
        for ($j=0; $j < count($mahasiswa); $j++) {
          if ($kuota >= $j+1) {
            DataPeminatan::where('id', $mahasiswa[$j]->id)
            ->update([
              'peminatan' => $pem->id,
              'status' => DB::raw('status+1'),
            ]);
            $terpakai++;
          }else{
            DataPeminatan::where('id', $mahasiswa[$j]->id)
            ->update([
              'peminatan' => null,
            ]);
          }
        }

        Peminatan::where('id', $pem->id)
        ->update([
          'sisa_kuota' => $kuota-$terpakai,
        ]);
      }
    }
    //end fase 3

    //fase 4 - memasukan yang tidak diterima ke dalam peminatan yang kosong
    $peminatan = Peminatan::all();
    foreach($peminatan as $pem)
    {
      $mahasiswa = DataPeminatan::where('peminatan', null)->get();
      if ($pem->sisa_kuota != 0) {
        foreach($mahasiswa as $mhs) {
          $nilai = DataNilai::where('nim', $mhs->users->nim)->first();
          if ($pem->id == "1") {
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $mhs->users->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == $pem->id) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->oop+$nilai->rpb+$nilai->web+$nilai->pi;
            $total = $skorKeprof + $skor;
          }else if($pem->id == "2") {
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $mhs->users->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == $pem->id) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->statistik+$nilai->matdis+$nilai->alpro+$nilai->strukdat;
            $total = $skorKeprof + $skor;
          }else if($pem->id == "3") {
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $mhs->users->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == $pem->id) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->oop+$nilai->apsi+$nilai->rpb+$nilai->web;
            $total = $skorKeprof + $skor;
          }else if($pem->id == "4") {
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $mhs->users->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == $pem->id) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->se+$nilai->po+$nilai->rpb+$nilai->scm;
            $total = $skorKeprof + $skor;
          }else if($pem->id == "5") {
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $mhs->users->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == $pem->id) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->ea+$nilai->apsi+$nilai->rpb+$nilai->basdat;
            $total = $skorKeprof + $skor;
          }else if($pem->id == "6") {
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $mhs->users->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == $pem->id) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->manjarkom+$nilai->se+$nilai->sisop+$nilai->msdm;
            $total = $skorKeprof + $skor;
          }else{
            $keprof = DataNilai::with('data_keprof')
            ->where('nim', $mhs->users->nim)
            ->first();
            $skorKeprof = 0;
            if ($keprof->data_keprof != null) {
              if ($keprof->data_keprof->id_keprof == $pem->id) {
                $skorKeprof = 2;
              }
            }
            $skor = $nilai->desjar+$nilai->manjarkom+$nilai->sisop+$nilai->manprosi;
            $total = $skorKeprof + $skor;
          }
          DataPeminatan::where('id', $mhs->id)->update([
            'skor_buang' => $total,
          ]);
        }

        $sisa = $pem->sisa_kuota;
        for ($j=0; $j < count($mahasiswa); $j++) {
          if ($sisa > 0) {
            DataPeminatan::where('id', $mahasiswa[$j]->id)
            ->update([
              'peminatan' => $pem->id,
              'status' => 99,
            ]);
          $sisa--;
          }else{
            break;
          }
        }
        Peminatan::where('id', $pem->id)
        ->update([
          'sisa_kuota' => $sisa,
        ]);
      }
    }
    //end fase 4

  }

  public function resetSeleksi()
  {
    DB::table('data_peminatan')->where('prioritas', 0)
    ->update([
      'status' => 0,
      'peminatan' => null,
      'updated_at' => Carbon::now(),
    ]);

    DB::table('peminatan')->update([
      'kuota' => 0,
      'sisa_kuota' => 0,
    ]);

    $peminatan = new PeminatanController();
    $hasil = $peminatan->kuota();

    if ($hasil == false) {
      Alert::error('Gagal', 'Pastikan data sudah dosen atau mahasiswa sudah terisi');
      return redirect()->back();
    }

    return true;
  }

  public function hasil()
  {
    $data['hasil'] = DataPeminatan::with('users','peminatan_1','peminatan_2','peminatans')
    ->get();
    $data['peminatan'] = Peminatan::all();
    return view('admin.hasil-seleksi', $data);
  }

      public function export()
	{
		return Excel::download(new HasilExport, 'hasilpeminatan.xlsx');
	}

  public function prioritas()
  {
    $peminatan = Peminatan::where('id_pembina', Auth::user()->id)->first();

    $prioritas = DataPeminatan::with('users','peminatan_1','users.data_nilai_mahasiswa.data_keprof')
    ->whereHas('peminatan_1', function($query){
      $query->where('id_pembina', Auth::user()->id);
    })
    ->whereHas('users.data_nilai_mahasiswa.data_keprof', function($query) use($peminatan){
      $query->where('id_keprof', $peminatan->id);
    })
    ->get();

    return view('dosen.prioritas', ['prioritas'=>$prioritas]);
  }

  public function proses_prioritas($id)
  {

    $server = DB::table('server')->get();
    if ($server[1]->status == 0) {
      Alert::error('Gagal', 'Fase prioritas sudah ditutup');
      return redirect('/prioritas');
    }

    $peminatan = Peminatan::where('id_pembina', Auth::user()->id);
    $jumlah_prioritas = $peminatan->first();

    if ($jumlah_prioritas->jumlah_prioritas == 3) {
      Alert::error('Gagal', 'Anda sudah memberikan maksimal prioritas kepada mahasiswa');
      return redirect()->back();
    }

    DataPeminatan::where('id', $id)
    ->update([
      'prioritas' => 1,
      'status' => 10,
      'peminatan' => $jumlah_prioritas->id,
      'updated_at' => Carbon::now(),
    ]);

    $peminatan->increment('jumlah_prioritas', 1);

    Alert::success('Berhasil', 'Berhasil memberi prioritas mahasiswa');
    return redirect()->back();
  }

  public function cancel_prioritas($id)
  {
      
    $server = DB::table('server')->get();
    if ($server[1]->status == 0) {
      Alert::error('Gagal', 'Fase prioritas sudah ditutup');
      return redirect('/prioritas');
    } 
      
    DataPeminatan::where('id', $id)
    ->update([
      'prioritas' => 0,
      'peminatan' => null,
      'status' => 0,
      'updated_at' => Carbon::now(),
    ]);

    $peminatan = Peminatan::where('id_pembina', Auth::user()->id);
    $peminatan->decrement('jumlah_prioritas', 1);

    Alert::success('Berhasil', 'Berhasil menghapus prioritas mahasiswa');
    return redirect()->back();
  }

}
