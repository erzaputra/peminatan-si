<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Mahasiswa
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (Auth::check()) {
        if (Auth::user()->id_role != 2) {
          return redirect('/home');
        }
      }else{
        return redirect('/home');
      }

      return $next($request);
    }
}
