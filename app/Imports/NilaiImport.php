<?php

namespace App\Imports;

use App\DataNilai;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class NilaiImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new DataNilai([
          'nim' => $row[0],
          'nama' => $row[1],
          'oop' => $row[2],
          'rpb' => $row[3],
          'pi' => $row[4],
          'apsi' => $row[5],
          'web' => $row[6],
          'statistik' => $row[7],
          'matdis' => $row[8],
          'alpro' => $row[9],
          'strukdat' => $row[10],
          'se' => $row[11],
          'po' => $row[12],
          'scm' => $row[13],
          'ea' => $row[14],
          'basdat' => $row[15],
          'manjarkom' => $row[16],
          'sisop' => $row[17],
          'msdm' => $row[18],
          'desjar' => $row[19],
          'manprosi' => $row[20],
        ]);
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
