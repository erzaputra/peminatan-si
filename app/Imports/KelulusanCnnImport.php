<?php

namespace App\Imports;

use App\DataNilai;
use App\DataPeminatan;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

class KelulusanCnnImport implements ToCollection, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $hasil = DataPeminatan::whereHas('users', function($query) use($row){
              $query->where('nim', $row[1]);
            })
            ->update([
              'status_lulus' => $row[22],
            ]);
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
