<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPeminatan extends Model
{
  protected $table = 'data_peminatan';

  public function users(){
    return $this->belongsTo('App\User', 'id_users', 'id');
  }

  public function peminatans(){
    return $this->belongsTo('App\Peminatan', 'peminatan', 'id');
  }

  public function peminatan_1(){
    return $this->belongsTo('App\Peminatan', 'id_peminatan_1', 'id');
  }

  public function peminatan_2(){
    return $this->belongsTo('App\Peminatan', 'id_peminatan_2', 'id');
  }

}
