<?php

namespace App\Exports;

use App\DataPeminatan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class HasilExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
      return view('hasilexport', [
          'hasil' => DataPeminatan::with('users','peminatans')->get()
      ]);
    }
}
