<?php

namespace App\Exports;

use App\DataNilai;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class NilaiExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
      return view('nilaiexport', [
          'hasil' => DataNilai::all()
      ]);
    }
}
