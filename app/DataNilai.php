<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataNilai extends Model
{
  protected $table = 'data_nilai_mahasiswa';

  protected $fillable= [

    'nim',
    'nama',
    'oop',
    'rpb',
    'pi',
    'apsi',
    'web',
    'statistik',
    'matdis',
    'alpro',
    'strukdat',
    'se',
    'po',
    'scm',
    'ea',
    'basdat',
    'manjarkom',
    'sisop',
    'msdm',
    'desjar',
    'manprosi',

  ];

  public function data_keprof(){
    return $this->belongsTo('App\Keprof', 'nim', 'nim');
  }

  public function users(){
    return $this->hasOne('App\User', 'nim', 'nim');
  }
}
