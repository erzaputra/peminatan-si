<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keprof extends Model
{
  protected $table = 'data_keprof';

  protected $fillable = [
    'nim','id_keprof'
  ];

  public function peminatan(){
    return $this->belongsTo('App\Peminatan', 'id_keprof', 'id');
  }

  public function data_nilai_mahasiswa(){
    return $this->hasOne('App\DataNilai', 'nim', 'nim');
  }

}
