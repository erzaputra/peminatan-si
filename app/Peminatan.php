<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminatan extends Model
{
  protected $table = 'peminatan';

  public function peminatan(){
    return $this->hasOne('App\Peminatan', 'peminatan', 'id');
  }

  public function peminatan_1(){
    return $this->hasOne('App\Peminatan', 'id_peminatan_1', 'id');
  }

  public function peminatan_2(){
    return $this->hasOne('App\Peminatan', 'id_peminatan_2', 'id');
  }

  public function data_keprof(){
    return $this->hasOne('App\Keprof', 'id_keprof', 'id');
  }

  public function data_dosen(){
    return $this->hasOne('App\Dosen', 'id_peminatan', 'id');
  }

  public function pembina(){
    return $this->hasOne('App\User', 'id_pembina', 'id');
  }

}
