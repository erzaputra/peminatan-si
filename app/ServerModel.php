<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServerModel extends Model
{
    protected $table = "server";
}
