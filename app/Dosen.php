<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
  protected $table = 'data_dosen';

  protected $fillable = [
    'id_peminatan','nama'
  ];

  public function peminatan(){
    return $this->belongsTo('App\Peminatan', 'id_peminatan', 'id');
  }

}
