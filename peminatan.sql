-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2019 at 11:21 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `peminatan`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_dosen`
--

CREATE TABLE `data_dosen` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_peminatan` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_keprof`
--

CREATE TABLE `data_keprof` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_keprof` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_nilai_mahasiswa`
--

CREATE TABLE `data_nilai_mahasiswa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oop` decimal(2,1) NOT NULL,
  `rpb` decimal(2,1) NOT NULL,
  `pi` decimal(2,1) NOT NULL,
  `apsi` decimal(2,1) NOT NULL,
  `web` decimal(2,1) NOT NULL,
  `statistik` decimal(2,1) NOT NULL,
  `matdis` decimal(2,1) NOT NULL,
  `alpro` decimal(2,1) NOT NULL,
  `strukdat` decimal(2,1) NOT NULL,
  `se` decimal(2,1) NOT NULL,
  `po` decimal(2,1) NOT NULL,
  `scm` decimal(2,1) NOT NULL,
  `ea` decimal(2,1) NOT NULL,
  `basdat` decimal(2,1) NOT NULL,
  `manjarkom` decimal(2,1) NOT NULL,
  `sisop` decimal(2,1) NOT NULL,
  `msdm` decimal(2,1) NOT NULL,
  `desjar` decimal(2,1) NOT NULL,
  `manprosi` decimal(2,1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_peminatan`
--

CREATE TABLE `data_peminatan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_users` bigint(20) UNSIGNED NOT NULL,
  `id_peminatan_1` bigint(20) UNSIGNED NOT NULL,
  `skor_peminatan_1` decimal(8,2) NOT NULL,
  `id_peminatan_2` bigint(20) UNSIGNED NOT NULL,
  `skor_peminatan_2` decimal(8,2) NOT NULL,
  `peminatan` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `prioritas` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(118, '2014_10_12_000000_create_users_table', 1),
(119, '2014_10_12_100000_create_password_resets_table', 1),
(120, '2019_08_19_000000_create_failed_jobs_table', 1),
(121, '2019_12_08_190018_create_roles_table', 1),
(122, '2019_12_08_190109_create_data_nilai_mahasiswa_table', 1),
(123, '2019_12_08_190701_create_data_peminatan_table', 1),
(124, '2019_12_08_190833_create_peminatan_table', 1),
(125, '2019_12_08_191047_add_relations', 1),
(126, '2019_12_08_201314_create_data_keprof_table', 1),
(127, '2019_12_08_201445_add_relations_keprof', 1),
(128, '2019_12_11_165213_create_data_dosen_table', 1),
(129, '2019_12_11_165617_relations_data_dosen_table', 1),
(130, '2019_12_28_160403_create_server_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `peminatan`
--

CREATE TABLE `peminatan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelompok` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `singkatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keprof` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kuota` int(11) NOT NULL DEFAULT 0,
  `sisa_kuota` int(11) NOT NULL DEFAULT 0,
  `id_pembina` bigint(20) UNSIGNED NOT NULL,
  `jumlah_prioritas` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `peminatan`
--

INSERT INTO `peminatan` (`id`, `name`, `kelompok`, `singkatan`, `keprof`, `kuota`, `sisa_kuota`, `id_pembina`, `jumlah_prioritas`, `created_at`, `updated_at`) VALUES
(1, 'Technopreneur', 'ESD', 'Techno', 'Technopreneur', 0, 0, 3, 0, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(2, 'Enterprise Data Management', 'ESD', 'EDM', 'Daspro', 0, 0, 4, 0, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(3, 'Enterprise System Development', 'ESD', 'ESD', 'EAD', 0, 0, 5, 0, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(4, 'Enterprise Resource Planning', 'ESA', 'ERP', 'ERP', 0, 0, 6, 0, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(5, 'Enterprise Architecture', 'ESA', 'EA', 'BPAD', 0, 0, 7, 0, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(6, 'Information System Management', 'ESA', 'ISM', 'ISM', 0, 0, 8, 0, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(7, 'Enterprise Infrastructure Management', 'ESA', 'EIM', 'Sisjar', 0, 0, 9, 0, '2019-12-30 22:19:17', '2019-12-30 22:19:17');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2019-12-30 22:19:16', '2019-12-30 22:19:16'),
(2, 'Mahasiswa', '2019-12-30 22:19:16', '2019-12-30 22:19:16'),
(3, 'Pembina Keprofesian', '2019-12-30 22:19:16', '2019-12-30 22:19:16'),
(4, 'Sekprodi', '2019-12-30 22:19:16', '2019-12-30 22:19:16'),
(5, 'Kaprodi', '2019-12-30 22:19:16', '2019-12-30 22:19:16');

-- --------------------------------------------------------

--
-- Table structure for table `server`
--

CREATE TABLE `server` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Admin',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `server`
--

INSERT INTO `server` (`id`, `nama_status`, `status`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Status Login', 0, 'Admin', '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(2, 'Status Pendaftaran', 0, 'Admin', '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(3, 'Status Kuota', 0, 'Admin', '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(4, 'Status Seleksi', 0, 'Admin', '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(5, 'Status Pengumuman', 0, 'Admin', '2019-12-30 22:19:17', '2019-12-30 22:19:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'avatar/user.png',
  `id_role` int(10) UNSIGNED NOT NULL DEFAULT 2,
  `bio` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `nim`, `email`, `email_verified_at`, `password`, `avatar`, `id_role`, `bio`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '1234567', 'admin@gmail.com', NULL, '$2y$10$FdDY8HxLDkgcTuMrXnj0lOPwRIA2ImhAPHOSVmuDI7It3stqN83Fa', 'avatar/user.png', 1, 'White Rabbit, \'but it seems to suit them!\' \'I haven\'t opened it yet,\' said Alice; \'all I know I do!\' said Alice to herself, as she added, \'and the moral of that dark hall, and wander about among.', NULL, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(2, 'Mahasiswa', '00000000', 'mahasiswa@gmail.com', NULL, '$2y$10$s81/0yOv2wDABc3TSfA1MuZMSGsJ/T6SNE2C44ROMSg5dllQmLVgG', 'avatar/user.png', 2, 'Hatter instead!\' CHAPTER VII. A Mad Tea-Party There was a different person then.\' \'Explain all that,\' said the Mouse, sharply and very angrily. \'A knot!\' said Alice, quite forgetting that she wanted.', NULL, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(3, 'Pembina Techno', '00000001', 'techno@gmail.com', NULL, '$2y$10$0kn/S6WlNhm5nWs8EZrW7uCQ9YF0cn5flW9yg2AhdQKOO3lvPRV8K', 'avatar/user.png', 3, 'Dormouse turned out, and, by the officers of the accident, all except the Lizard, who seemed ready to play croquet.\' Then they all quarrel so dreadfully one can\'t hear oneself speak--and they don\'t.', NULL, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(4, 'Pembina EDM', '00000002', 'edm@gmail.com', NULL, '$2y$10$cXmSQDeXw5T5ZuzohSmDle3104hYeqPe4kK1IRl/gMFYx2/yccQ3O', 'avatar/user.png', 3, 'Shall I try the effect: the next thing was waving its tail about in the same thing a Lobster Quadrille The Mock Turtle went on talking: \'Dear, dear! How queer everything is to-day! And yesterday.', NULL, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(5, 'Pembina ESD', '00000003', 'esd@gmail.com', NULL, '$2y$10$22eisPOqxiK4qGjGvxRYLOxXAQsd/1hJmQ5y4hZ.UF9xNjq.2yxuy', 'avatar/user.png', 3, 'Edgar Atheling to meet William and offer him the crown. William\'s conduct at first she would get up and beg for its dinner, and all sorts of things--I can\'t remember half of anger, and tried to open.', NULL, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(6, 'Pembina ERP', '00000004', 'erp@gmail.com', NULL, '$2y$10$HX1TzmORT8AicVApvWU0cefi8y3skQBZWH5AQllBlrblQhl6qSDSy', 'avatar/user.png', 3, 'I chose,\' the Duchess sang the second thing is to do it?\' \'In my youth,\' said the Hatter. \'Does YOUR watch tell you my history, and you\'ll understand why it is you hate--C and D,\' she added in a.', NULL, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(7, 'Pembina EA', '00000005', 'ea@gmail.com', NULL, '$2y$10$UwhyA032632gbHFnMK4hheGiozwetl6xiwbl07UVanbdcgS19Y3L2', 'avatar/user.png', 3, 'This is the same size for going through the little creature down, and felt quite unhappy at the proposal. \'Then the Dormouse followed him: the March Hare went on. \'Would you tell me, please, which.', NULL, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(8, 'Pembina ISM', '00000006', 'ism@gmail.com', NULL, '$2y$10$R4eEAKL4oyMUHUlw6xitce3Jv3UGx3kO7bA1fQMfjnA9IQ5DFpb4y', 'avatar/user.png', 3, 'THAT\'S a good way off, and she felt sure it would be grand, certainly,\' said Alice, always ready to play croquet with the Queen,\' and she went back to the Queen. \'I never went to work very.', NULL, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(9, 'Pembina EIM', '00000007', 'eim@gmail.com', NULL, '$2y$10$rKl5IhZzVcdNg7iHs0FD3.bpKmKsM2a54u6AfHYXN5JC0GccNyH46', 'avatar/user.png', 3, 'Caterpillar The Caterpillar and Alice looked down at her rather inquisitively, and seemed not to her, so she went to school in the window?\' \'Sure, it\'s an arm for all that.\' \'Well, it\'s got no.', NULL, '2019-12-30 22:19:17', '2019-12-30 22:19:17'),
(10, 'Kaprodi', '123456', 'kaprodi@gmail.com', NULL, '$2y$10$u8zC2Bv6iEE9vvMCXC9BbO08G3N0Zc8psWqTeGI5VGNPjvXy0UdJi', 'avatar/user.png', 5, 'The Mouse looked at the thought that SOMEBODY ought to have the experiment tried. \'Very true,\' said the Cat. \'Do you play croquet with the strange creatures of her voice. Nobody moved. \'Who cares.', NULL, '2019-12-30 22:19:17', '2019-12-30 22:19:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_dosen`
--
ALTER TABLE `data_dosen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_dosen_id_peminatan_foreign` (`id_peminatan`);

--
-- Indexes for table `data_keprof`
--
ALTER TABLE `data_keprof`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_keprof_nim_unique` (`nim`),
  ADD KEY `data_keprof_id_keprof_foreign` (`id_keprof`);

--
-- Indexes for table `data_nilai_mahasiswa`
--
ALTER TABLE `data_nilai_mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_nilai_mahasiswa_nim_unique` (`nim`);

--
-- Indexes for table `data_peminatan`
--
ALTER TABLE `data_peminatan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_peminatan_id_users_unique` (`id_users`),
  ADD KEY `data_peminatan_id_peminatan_1_foreign` (`id_peminatan_1`),
  ADD KEY `data_peminatan_id_peminatan_2_foreign` (`id_peminatan_2`),
  ADD KEY `data_peminatan_peminatan_foreign` (`peminatan`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `peminatan`
--
ALTER TABLE `peminatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `server`
--
ALTER TABLE `server`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_nim_unique` (`nim`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_role_foreign` (`id_role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_dosen`
--
ALTER TABLE `data_dosen`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_keprof`
--
ALTER TABLE `data_keprof`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_nilai_mahasiswa`
--
ALTER TABLE `data_nilai_mahasiswa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_peminatan`
--
ALTER TABLE `data_peminatan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `peminatan`
--
ALTER TABLE `peminatan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `server`
--
ALTER TABLE `server`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_dosen`
--
ALTER TABLE `data_dosen`
  ADD CONSTRAINT `data_dosen_id_peminatan_foreign` FOREIGN KEY (`id_peminatan`) REFERENCES `peminatan` (`id`);

--
-- Constraints for table `data_keprof`
--
ALTER TABLE `data_keprof`
  ADD CONSTRAINT `data_keprof_id_keprof_foreign` FOREIGN KEY (`id_keprof`) REFERENCES `peminatan` (`id`),
  ADD CONSTRAINT `data_keprof_nim_foreign` FOREIGN KEY (`nim`) REFERENCES `data_nilai_mahasiswa` (`nim`);

--
-- Constraints for table `data_peminatan`
--
ALTER TABLE `data_peminatan`
  ADD CONSTRAINT `data_peminatan_id_peminatan_1_foreign` FOREIGN KEY (`id_peminatan_1`) REFERENCES `peminatan` (`id`),
  ADD CONSTRAINT `data_peminatan_id_peminatan_2_foreign` FOREIGN KEY (`id_peminatan_2`) REFERENCES `peminatan` (`id`),
  ADD CONSTRAINT `data_peminatan_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `data_peminatan_peminatan_foreign` FOREIGN KEY (`peminatan`) REFERENCES `peminatan` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_role_foreign` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
