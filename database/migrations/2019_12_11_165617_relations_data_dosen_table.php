<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RelationsDataDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('data_dosen', function (Blueprint $table) {
        $table->foreign('id_peminatan')->references('id')->on('peminatan');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('data_dosen', function (Blueprint $table) {
        $table->dropForeign('data_dosen_id_peminatan_foreign');
      });
    }
}
