<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeminatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peminatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('kelompok');
            $table->string('singkatan');
            $table->string('keprof');
            $table->integer('kuota')->default(0);
            $table->integer('sisa_kuota')->default(0);
            $table->bigInteger('id_pembina')->unsigned();
            $table->integer('jumlah_prioritas')->default(0);
            $table->string('matkul')->nullable();
            $table->string('kode_matkul')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peminatan');
    }
}
