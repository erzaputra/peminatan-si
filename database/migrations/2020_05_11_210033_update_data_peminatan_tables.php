<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDataPeminatanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_peminatan', function(Blueprint $table){
          $table->string('status_lulus')->nullable()->after('skor_buang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('data_peminatan', function(Blueprint $table){
        $table->dropColumn('status_lulus');
      });
    }
}
