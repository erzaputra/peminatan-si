<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelations extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->foreign('id_role')->references('id')->on('roles');
    });

    Schema::table('data_peminatan', function (Blueprint $table) {
      $table->foreign('id_users')->references('id')->on('users');
      $table->foreign('id_peminatan_1')->references('id')->on('peminatan');
      $table->foreign('id_peminatan_2')->references('id')->on('peminatan');
      $table->foreign('peminatan')->references('id')->on('peminatan');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->dropForeign('users_id_role_foreign');
    });

    Schema::table('data_peminatan', function (Blueprint $table) {
      $table->dropForeign('data_peminatan_id_users_foreign');
      $table->dropForeign('data_peminatan_id_peminatan_1_foreign');
      $table->dropForeign('data_peminatan_id_peminatan_2_foreign');
      $table->dropForeign('data_peminatan_peminatan_foreign');
    });
  }
}
