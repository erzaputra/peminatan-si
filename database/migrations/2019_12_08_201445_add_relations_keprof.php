<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationsKeprof extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('data_keprof', function (Blueprint $table) {
        $table->foreign('id_keprof')->references('id')->on('peminatan');
        $table->foreign('nim')->references('nim')->on('data_nilai_mahasiswa');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('data_keprof', function (Blueprint $table) {
        $table->dropForeign('data_keprof_id_keprof_foreign');
        $table->dropForeign('data_keprof_nim_foreign');
      });
    }
}
