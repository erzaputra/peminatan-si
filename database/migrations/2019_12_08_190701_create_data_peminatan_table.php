<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPeminatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_peminatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_users')->unsigned()->unique();
            $table->biginteger('id_peminatan_1')->unsigned();
            $table->decimal('skor_peminatan_1');
            $table->biginteger('id_peminatan_2')->unsigned();
            $table->decimal('skor_peminatan_2');
            $table->biginteger('peminatan')->unsigned()->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('prioritas')->default(0);
            $table->decimal('skor_buang')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_peminatan');
    }
}
