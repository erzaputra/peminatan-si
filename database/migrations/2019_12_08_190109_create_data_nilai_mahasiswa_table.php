<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataNilaiMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_nilai_mahasiswa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nim')->unique();
            $table->string('nama');
            $table->decimal('oop',2,1);
            $table->decimal('rpb',2,1);
            $table->decimal('pi',2,1);
            $table->decimal('apsi',2,1);
            $table->decimal('web',2,1);
            $table->decimal('statistik',2,1);
            $table->decimal('matdis',2,1);
            $table->decimal('alpro',2,1);
            $table->decimal('strukdat',2,1);
            $table->decimal('se',2,1);
            $table->decimal('po',2,1);
            $table->decimal('scm',2,1);
            $table->decimal('ea',2,1);
            $table->decimal('basdat',2,1);
            $table->decimal('manjarkom',2,1);
            $table->decimal('sisop',2,1);
            $table->decimal('msdm',2,1);
            $table->decimal('desjar',2,1);
            $table->decimal('manprosi',2,1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_nilai_mahasiswa');
    }
}
