<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PeminatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('peminatan')->insert([
          'name' => 'Technopreneur',
          'kelompok' => 'ESD',
          'singkatan' => 'Techno',
          'keprof' => 'Technopreneur',
          'id_pembina' => 3,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('peminatan')->insert([
          'name' => 'Enterprise Data Management',
          'kelompok' => 'ESD',
          'singkatan' => 'EDM',
          'keprof' => 'Daspro',
          'id_pembina' => 4,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('peminatan')->insert([
          'name' => 'Enterprise System Development',
          'kelompok' => 'ESD',
          'singkatan' => 'ESD',
          'keprof' => 'EAD',
          'id_pembina' => 5,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('peminatan')->insert([
          'name' => 'Enterprise Resource Planning',
          'kelompok' => 'ESA',
          'singkatan' => 'ERP',
          'keprof' => 'ERP',
          'id_pembina' => 6,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('peminatan')->insert([
          'name' => 'Enterprise Architecture',
          'kelompok' => 'ESA',
          'singkatan' => 'EA',
          'keprof' => 'BPAD',
          'id_pembina' => 7,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('peminatan')->insert([
          'name' => 'Information System Management',
          'kelompok' => 'ESA',
          'singkatan' => 'ISM',
          'keprof' => 'ISM',
          'id_pembina' => 8,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('peminatan')->insert([
          'name' => 'Enterprise Infrastructure Management',
          'kelompok' => 'ESA',
          'singkatan' => 'EIM',
          'keprof' => 'Sisjar',
          'id_pembina' => 9,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);
    }
}
