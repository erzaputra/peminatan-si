<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ServerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table("server")->insert([
        'nama_status' => 'Status Login',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
      DB::table("server")->insert([
        'nama_status' => 'Status Pendaftaran',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
      DB::table("server")->insert([
        'nama_status' => 'Status Kuota',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
      DB::table("server")->insert([
        'nama_status' => 'Status Seleksi',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
      DB::table("server")->insert([
        'nama_status' => 'Status Pengumuman',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
    }
}
