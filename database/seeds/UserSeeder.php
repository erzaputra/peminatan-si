<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\DataNilai;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        DB::table('users')->insert([
          'name' => 'Admin',
          'nim' => '1234567',
          'email' => 'admin@gmail.com',
          'password' => bcrypt('admin'),
          'id_role' => 1,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
          'name' => 'Mahasiswa',
          'nim' => '00000000',
          'email' => 'mahasiswa@gmail.com',
          'password' => bcrypt('mahasiswa'),
          'id_role' => 2,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
          'name' => 'Pembina Techno',
          'nim' => '00000001',
          'email' => 'techno@gmail.com',
          'password' => bcrypt('techno'),
          'id_role' => 3,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
          'name' => 'Pembina EDM',
          'nim' => '00000002',
          'email' => 'edm@gmail.com',
          'password' => bcrypt('edm'),
          'id_role' => 3,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
          'name' => 'Pembina ESD',
          'nim' => '00000003',
          'email' => 'esd@gmail.com',
          'password' => bcrypt('esd'),
          'id_role' => 3,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
          'name' => 'Pembina ERP',
          'nim' => '00000004',
          'email' => 'erp@gmail.com',
          'password' => bcrypt('erp'),
          'id_role' => 3,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
          'name' => 'Pembina EA',
          'nim' => '00000005',
          'email' => 'ea@gmail.com',
          'password' => bcrypt('ea'),
          'id_role' => 3,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
          'name' => 'Pembina ISM',
          'nim' => '00000006',
          'email' => 'ism@gmail.com',
          'password' => bcrypt('ism'),
          'id_role' => 3,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
          'name' => 'Pembina EIM',
          'nim' => '00000007',
          'email' => 'eim@gmail.com',
          'password' => bcrypt('eim'),
          'id_role' => 3,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
          'name' => 'Kaprodi',
          'nim' => '123456',
          'email' => 'kaprodi@gmail.com',
          'password' => bcrypt('kaprodi'),
          'id_role' => 5,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);
    }
}
