<?php

use Illuminate\Database\Seeder;
use App\User;
use App\DataNilai;
use Carbon\Carbon;

class DataPeminatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $faker = Faker\Factory::create();

      $students = DataNilai::all();
      foreach($students as $student)
      {
        DB::table('users')->insert([
          'name' => $student->nama,
          'nim' => $student->nim,
          'email' => $student->nim.'@gmail.com',
          'password' => bcrypt($student->nim),
          'id_role' => 2,
          'bio' => $faker->realText(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);
      }

      $users = User::all();
      for ($i=11; $i <= 252; $i++) {
        DB::table('data_peminatan')->insert([
          'id_users' => $i,
          'id_peminatan_1' => $faker->numberBetween($min = 1, $max = 3),
          'skor_peminatan_1' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 19),
          'id_peminatan_2' => $faker->numberBetween($min = 4, $max = 7),
          'skor_peminatan_2' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 19),
        ]);
      }

      for ($i=253; $i <= count($users); $i++) {
        DB::table('data_peminatan')->insert([
          'id_users' => $i,
          'id_peminatan_1' => $faker->numberBetween($min = 4, $max = 7),
          'skor_peminatan_1' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 19),
          'id_peminatan_2' => $faker->numberBetween($min = 1, $max = 3),
          'skor_peminatan_2' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 19),
        ]);
      }
    }
}
