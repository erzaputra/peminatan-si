<table>
  <tr>
    <td>No</td>
    <td>NIM</td>
    <td>Nama</td>
    <td>Techno</td>
    <td>EDM</td>
    <td>ESD</td>
    <td>ERP</td>
    <td>EA</td>
    <td>ISM</td>
    <td>EIM</td>
  </tr>
  @foreach($hasil as $data)
  <tr>
    <td>{{ $loop->iteration }}</td>
    <td>{{ $data->nim }}</td>
    <td>{{ ucfirst($data->nama) }}</td>
    <td>{{ $techno[$loop->index] }}</td>
    <td>{{ $EDM[$loop->index] }}</td>
    <td>{{ $ESD[$loop->index] }}</td>
    <td>{{ $ERP[$loop->index] }}</td>
    <td>{{ $EA[$loop->index] }}</td>
    <td>{{ $ISM[$loop->index] }}</td>
    <td>{{ $EIM[$loop->index] }}</td>
  </tr>
  @endforeach
</table>
