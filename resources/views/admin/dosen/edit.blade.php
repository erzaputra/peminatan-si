@extends('layouts.app-dashboard')

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Tambah Data Dosen
    </div>
    <div class="card-body">
      <form action="{{ url('/master/data-dosen/edit/'.$dosen->id.'/proses') }}" method="post">
        @csrf
        <div class="form-group">
          <label>Nama Dosen</label>
          <input name="nama" type="text" class="form-control @error('nama') is-invalid @enderror" placeholder="Ex : Rachmadita Andreswari" value="{{ $dosen->nama }}">
          @error('nama')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Peminatan</label>
          <select class="form-control @error('peminatan') is-invalid @enderror" name="peminatan">
            <option value="">Pilih Peminatan</option>
            @foreach($peminatan as $data)
            <option value="{{ $data->id }}" {{ $dosen->id_peminatan ==  $data->id ? 'selected' : '' }}>{{ $data->name }}</option>
            @endforeach
          </select>
          @error('peminatan')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
