@extends('layouts.app-dashboard')

@push('css')
<!-- Custom styles for this page -->
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Data Dosen
      <a href="{{ url('/master/data-dosen/tambah') }}" class="d-none d-sm-inline-block btn btn-sm btn-light shadow-sm float-right text-primary"><i class="fas fa-plus fa-sm text-primary"></i> Tambah Data</a>
      <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm float-right text-white mr-3" data-toggle="modal" data-target="#inputModal"><i class="fas fa-plus fa-sm text-white"></i> Import Excel</a>
      <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-light shadow-sm float-right text-primary mr-3" data-toggle="modal" data-target="#templateModal"><i class="fas fa-download fa-sm text-primary"></i> Download Template</a>
      <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm float-right text-white mr-3" data-toggle="modal" data-target="#deleteModal">Truncate Data</a>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Kelompok</th>
              <th>Peminatan</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($dosen as $data)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $data->nama }}</td>
              <td>{{ $data->peminatan->kelompok }}</td>
              <td>{{ $data->peminatan->name }}</td>
              <td>
                <form class="" action="{{ url('/master/data-dosen/delete/'.$data->id) }}" method="post">
                  @csrf
                  <button type="submit" name="delete" class="btn btn-danger" >Hapus</button>
                  <a href="{{ url('/master/data-dosen/edit/'.$data->id) }}" class="btn btn-primary" >Ubah</a>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="inputModal" tabindex="-1" role="dialog" aria-labelledby="inputModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-dark" id="exampleModalLabel">Import Data Dosen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <small class="text-center mt-3">Pastikan Excel Anda Sesuai Template</small>
      <form id="addinput" action="{{ url('/master/data-dosen/import') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <div class="input-group mb-3">
              <div class="custom-file">
                <input name="file" type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="inputGroupFile01">Pilih File</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-dark" id="exampleModalLabel">Apakah anda yakin ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('/master/data-dosen/truncate') }}" method="post">
        @csrf
        <div class="modal-body">
          <p>Data yang dihapus tidak dapat dikembalikan lagi</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="templateModal" tabindex="-1" role="dialog" aria-labelledby="templateModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-dark" id="exampleModalLabel">Download Template Dosen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Berikut <span class="font-weight-bold">List ID Peminatan</span></p>
        <table class="table">
          @foreach($peminatan as $dataPeminatan)
          <tr>
            <td>{{ $dataPeminatan->name }}</td>
            <td>:</td>
            <td>{{ $dataPeminatan->id }}</td>
          </tr>
          @endforeach
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="{{ asset('template/template_dosen.xlsx') }}" type="submit" class="btn btn-primary"><i class="fas fa-download fa-sm text-white"></i> Download Template</a>
        </div>
      </div>
    </div>
  </div>

  @endsection

  @push('script')
  <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

  <script type="text/javascript">
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
</script>
@endpush
