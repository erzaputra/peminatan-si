@extends('layouts.app-dashboard')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Mahasiswa</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $mahasiswa->count() }} Mahasiswa</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Dosen</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $dosen->count() }} Dosen</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Peminatan</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $peminatan->count() }} Peminatan</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xl-8 col-lg-7">
      <div class="card shadow mb-4">
        <div class="card-header bg-primary text-white">
          Panel Server
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Status</th>
                  <th>Action</th>
                  <th>Last Edited By</th>
                  <th>Last Edited On</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{ $server[0]->nama_status }}</td>
                  <td>
                    <form class="" action="{{ url('/server/'.$server[0]->id.'/status-update') }}" method="post">
                      @csrf
                      <button type="submit" name="button" class="btn {{ $server[0]->status == 0 ? 'btn-success' : 'btn-danger' }}">{{ $server[0]->status == 0 ? "Enable" : "Disable" }}</button>
                    </form>
                  </td>
                  <td>{{ $server[0]->updated_by }}</td>
                  <td>{{ $server[0]->updated_at }}</td>
                </tr>
                <tr>
                  <td>{{ $server[1]->nama_status }}</td>
                  <td>
                    <form class="" action="{{ url('/server/'.$server[1]->id.'/status-update') }}" method="post">
                      @csrf
                      <button type="submit" name="button" class="btn {{ $server[1]->status == 0 ? 'btn-success' : 'btn-danger' }}">{{ $server[1]->status == 0 ? "Enable" : "Disable" }}</button>
                    </form>
                  </td>
                  <td>{{ $server[1]->updated_by }}</td>
                  <td>{{ $server[1]->updated_at }}</td>
                </tr>
                <tr>
                  <td>{{ $server[2]->nama_status }}</td>
                  <td>
                    <form class="" action="{{ url('/server/'.$server[2]->id.'/generate') }}" method="post">
                      @csrf
                      <button type="submit" name="button" class="btn {{ $server[2]->status == 0 ? 'btn-success' : 'btn-danger' }}">{{ $server[2]->status == 0 ? "Generate" : "Reset" }}</button>
                    </form>
                  </td>
                  <td>{{ $server[1]->updated_by }}</td>
                  <td>{{ $server[1]->updated_at }}</td>
                </tr>
                <tr>
                  <td>{{ $server[3]->nama_status }}</td>
                  <td>
                    <form class="" action="{{ url('/server/'.$server[3]->id.'/generate') }}" method="post">
                      @csrf
                      <button type="submit" name="button" class="btn {{ $server[3]->status == 0 ? 'btn-success' : 'btn-danger' }}">{{ $server[3]->status == 0 ? "Generate" : "Reset" }}</button>
                    </form>
                  </td>
                  <td>{{ $server[3]->updated_by }}</td>
                  <td>{{ $server[3]->updated_at }}</td>
                </tr>
                <tr>
                  <td>{{ $server[4]->nama_status }}</td>
                  <td>
                    <form class="" action="{{ url('/server/'.$server[4]->id.'/status-update') }}" method="post">
                      @csrf
                      <button type="submit" name="button" class="btn {{ $server[4]->status == 0 ? 'btn-success' : 'btn-danger' }}">{{ $server[4]->status == 0 ? "Enable" : "Disable" }}</button>
                    </form>
                  </td>
                  <td>{{ $server[4]->updated_by }}</td>
                  <td>{{ $server[4]->updated_at }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-lg-5">
      <div class="card shadow mb-4">
        <div class="card-header bg-primary text-white">
          Status Server
        </div>
        <div class="card-body">
          <div class="login mb-3">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ $server[0]->nama_status }}</div>
            <div class="h5 mb-0 font-weight-bold {{ $server[0]->status == 0 ? 'text-danger' : 'text-success' }}">{{ $server[0]->status == 0 ? "Disable" : "Enable" }}</div>
          </div>
          <div class="register mb-3">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ $server[1]->nama_status }}</div>
            <div class="h5 mb-0 font-weight-bold {{ $server[1]->status == 0 ? 'text-danger' : 'text-success' }}">{{ $server[1]->status == 0 ? "Disable" : "Enable" }}</div>
          </div>
          <div class="Kuota mb-3">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ $server[2]->nama_status }}</div>
            <div class="h5 mb-0 font-weight-bold {{ $server[2]->status == 0 ? 'text-danger' : 'text-success' }}">{{ $server[2]->status == 0 ? "Belum Generate" : "Sudah Generate" }}</div>
          </div>
          <div class="seleksi mb-3">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ $server[3]->nama_status }}</div>
            <div class="h5 mb-0 font-weight-bold {{ $server[3]->status == 0 ? 'text-danger' : 'text-success' }}">{{ $server[3]->status == 0 ? "Belum Generate" : "Sudah Generate" }}</div>
          </div>
          <div class="pengumuman">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ $server[4]->nama_status }}</div>
            <div class="h5 mb-0 font-weight-bold {{ $server[4]->status == 0 ? 'text-danger' : 'text-success' }}">{{ $server[4]->status == 0 ? "Disable" : "Enable" }}</div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

@endsection

@push('script')

@endpush
