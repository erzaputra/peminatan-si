@extends('layouts.app-dashboard')

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Tambah Data Nilai Mahasiswa
    </div>
    <div class="card-body">
      <form action="{{ url('/master/data-nilai-mahasiswa/tambah/proses') }}" method="post">
        @csrf
        <div class="form-group">
          <label>NIM Mahasiswa</label>
          <input name="nim" type="text" class="form-control @error('nim') is-invalid @enderror" placeholder="Ex : 1202100000" value="{{ old('nim') }}">
          @error('nim')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Nama Mahasiswa</label>
          <input name="nama" type="text" class="form-control @error('nama') is-invalid @enderror" placeholder="Ex : Erza Putra" value="{{ old('nama') }}">
          @error('nama')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Object Oriented Programming (OOP)</label>
          <select class="form-control @error('oop') is-invalid @enderror" name="oop">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('oop') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('oop') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('oop') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('oop') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('oop') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('oop') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('oop') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('oop') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('oop')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Rekayasa Proses Bisnis (RPB)</label>
          <select class="form-control @error('rpb') is-invalid @enderror" name="rpb">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('rpb') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('rpb') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('rpb') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('rpb') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('rpb') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('rpb') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('rpb') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('rpb') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('rpb')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Perancangan Interamanprosi (PI)</label>
          <select class="form-control @error('pi') is-invalid @enderror" name="pi">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('pi') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('pi') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('pi') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('pi') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('pi') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('pi') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('pi') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('pi') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('pi')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Analisis Perancangan Sistem Informasi (APSI)</label>
          <select class="form-control @error('apsi') is-invalid @enderror" name="apsi">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('apsi') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('apsi') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('apsi') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('apsi') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('apsi') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('apsi') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('apsi') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('apsi') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('apsi')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Web Application Development (WAD)</label>
          <select class="form-control @error('web') is-invalid @enderror" name="web">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('web') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('web') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('web') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('web') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('web') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('web') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('web') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('web') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('web')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Statistik</label>
          <select class="form-control @error('statistik') is-invalid @enderror" name="statistik">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('statistik') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('statistik') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('statistik') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('statistik') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('statistik') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('statistik') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('statistik') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('statistik') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('statistik')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Matematika Diskrit (Matdis)</label>
          <select class="form-control @error('matdis') is-invalid @enderror" name="matdis">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('matdis') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('matdis') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('matdis') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('matdis') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('matdis') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('matdis') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('matdis') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('matdis') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('matdis')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Algoritma dan Pemrograman (Alpro)</label>
          <select class="form-control @error('alpro') is-invalid @enderror" name="alpro">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('alpro') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('alpro') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('alpro') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('alpro') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('alpro') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('alpro') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('alpro') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('alpro') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('alpro')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Struktur Data (Strukdat)</label>
          <select class="form-control @error('strukdat') is-invalid @enderror" name="strukdat">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('strukdat') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('strukdat') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('strukdat') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('strukdat') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('strukdat') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('strukdat') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('strukdat') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('strukdat') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('strukdat')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>System Enterprise (SE)</label>
          <select class="form-control @error('se') is-invalid @enderror" name="se">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('se') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('se') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('se') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('se') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('se') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('se') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('se') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('se') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('se')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Perilaku Organisasi (PO)</label>
          <select class="form-control @error('po') is-invalid @enderror" name="po">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('po') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('po') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('po') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('po') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('po') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('po') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('po') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('po') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('po')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Supply Chain Management (SCM)</label>
          <select class="form-control @error('scm') is-invalid @enderror" name="scm">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('scm') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('scm') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('scm') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('scm') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('scm') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('scm') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('scm') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('scm') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('scm')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Enterprise Application (EA)</label>
          <select class="form-control @error('ea') is-invalid @enderror" name="ea">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('ea') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('ea') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('ea') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('ea') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('ea') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('ea') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('ea') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('ea') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('ea')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Basis Data (Basdat)</label>
          <select class="form-control @error('basdat') is-invalid @enderror" name="basdat">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('basdat') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('basdat') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('basdat') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('basdat') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('basdat') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('basdat') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('basdat') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('basdat') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('basdat')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Management Jaringan Komputer (Manjarkom)</label>
          <select class="form-control @error('manjarkom') is-invalid @enderror" name="manjarkom">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('manjarkom') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('manjarkom') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('manjarkom') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('manjarkom') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('manjarkom') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('manjarkom') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('manjarkom') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('manjarkom') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('manjarkom')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Sistem Operasi (Sisop)</label>
          <select class="form-control @error('sisop') is-invalid @enderror" name="sisop">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('sisop') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('sisop') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('sisop') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('sisop') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('sisop') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('sisop') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('sisop') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('sisop') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('sisop')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Management Sumber Daya Manusia (MSDM)</label>
          <select class="form-control @error('msdm') is-invalid @enderror" name="msdm">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('msdm') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('msdm') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('msdm') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('msdm') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('msdm') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('msdm') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('msdm') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('msdm') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('msdm')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Desain Jaringan (Desjar)</label>
          <select class="form-control @error('desjar') is-invalid @enderror" name="desjar">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('desjar') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('desjar') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('desjar') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('desjar') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('desjar') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('desjar') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('desjar') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('desjar') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('desjar')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Manajemen Proyek Sistem Informasi (Manprosi)</label>
          <select class="form-control @error('manprosi') is-invalid @enderror" name="manprosi">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ old('manprosi') == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ old('manprosi') == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ old('manprosi') == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ old('manprosi') == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ old('manprosi') == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ old('manprosi') == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ old('manprosi') == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ old('manprosi') == '0' ? 'selected' : '' }}>T</option>
          </select>
          @error('manprosi')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
