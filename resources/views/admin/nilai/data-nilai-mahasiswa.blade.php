@extends('layouts.app-dashboard')

@push('css')
<!-- Custom styles for this page -->
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Data Nilai Mahasiswa
      <a href="{{ url('/master/data-nilai-mahasiswa/tambah') }}" class="d-none d-sm-inline-block btn btn-sm btn-light shadow-sm float-right text-primary"><i class="fas fa-plus fa-sm text-primary"></i> Tambah Data</a>
      <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm float-right text-white mr-3" data-toggle="modal" data-target="#inputModal"><i class="fas fa-plus fa-sm text-white"></i> Import Excel</a>
      <a href="{{ asset('template/template_nilai.xlsx') }}" class="d-none d-sm-inline-block btn btn-sm btn-light shadow-sm float-right text-primary mr-3"><i class="fas fa-download fa-sm text-primary"></i> Download Template</a>
      <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm float-right text-white mr-3" data-toggle="modal" data-target="#deleteModal">Truncate Data</a>
    </div>
    <div class="card-body">
      <table class="table table-bordered table-responsive" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>OOP</th>
            <th>RPB</th>
            <th>PI</th>
            <th>APSI</th>
            <th>Web</th>
            <th>Statistik</th>
            <th>Matdis</th>
            <th>Alpro</th>
            <th>Strukdat</th>
            <th>SE</th>
            <th>PO</th>
            <th>SCM</th>
            <th>EA</th>
            <th>Basdat</th>
            <th>Manjarkom</th>
            <th>Sisop</th>
            <th>MSDM</th>
            <th>Desjar</th>
            <th>Manprosi</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($nilai as $data)
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $data->nim }}</td>
            <td>{{ $data->nama }}</td>
            <td>{{ $data->oop }}</td>
            <td>{{ $data->rpb }}</td>
            <td>{{ $data->pi }}</td>
            <td>{{ $data->apsi }}</td>
            <td>{{ $data->web }}</td>
            <td>{{ $data->statistik }}</td>
            <td>{{ $data->matdis }}</td>
            <td>{{ $data->alpro }}</td>
            <td>{{ $data->strukdat }}</td>
            <td>{{ $data->se }}</td>
            <td>{{ $data->po }}</td>
            <td>{{ $data->scm }}</td>
            <td>{{ $data->ea }}</td>
            <td>{{ $data->basdat }}</td>
            <td>{{ $data->manjarkom }}</td>
            <td>{{ $data->sisop }}</td>
            <td>{{ $data->msdm }}</td>
            <td>{{ $data->desjar }}</td>
            <td>{{ $data->manprosi }}</td>
            <td>
              <form class="" action="{{ url('/master/data-nilai-mahasiswa/delete/'.$data->id) }}" method="post">
                @csrf
                <button type="submit" name="delete" class="btn btn-danger" >Hapus</button>
                <a href="{{ url('/master/data-nilai-mahasiswa/edit/'.$data->id) }}" class="btn btn-primary" >Ubah</a>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="inputModal" tabindex="-1" role="dialog" aria-labelledby="inputModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-dark" id="exampleModalLabel">Import Data Mahasiswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <small class="text-center mt-3">Pastikan Excel Anda Sesuai Template</small>
      <form id="addinput" action="{{ url('/master/data-nilai-mahasiswa/import') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <div class="input-group mb-3">
              <div class="custom-file">
                <input name="file" type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="inputGroupFile01">Pilih File</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-dark" id="exampleModalLabel">Apakah anda yakin ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('/master/data-nilai-mahasiswa/truncate') }}" method="post">
        @csrf
        <div class="modal-body">
          <p>Data yang dihapus tidak dapat dikembalikan lagi</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

@push('script')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page level custom scripts -->
<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

<script type="text/javascript">
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endpush
