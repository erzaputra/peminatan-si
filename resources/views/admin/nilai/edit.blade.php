@extends('layouts.app-dashboard')

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Edit Data Nilai Mahasiswa
    </div>
    <div class="card-body">
      <form action="{{ url('/master/data-nilai-mahasiswa/edit/'.$nilai->id.'/proses') }}" method="post">
        @csrf
        <div class="form-group">
          <label>NIM Mahasiswa</label>
          <input name="nim" type="text" class="form-control @error('nim') is-invalid @enderror" placeholder="Ex : 1202100000" value="{{ $nilai->nim }}">
          @error('nim')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Nama Mahasiswa</label>
          <input name="nama" type="text" class="form-control @error('nama') is-invalid @enderror" placeholder="Ex : Erza Putra" value="{{ $nilai->nama }}">
          @error('nama')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Object Oriented Programming (OOP)</label>
          <select class="form-control @error('oop') is-invalid @enderror" name="oop">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->oop == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->oop == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->oop == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->oop == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->oop == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->oop == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->oop == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->oop }}>T</option>
          </select>
          @error('oop')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Rekayasa Proses Bisnis (RPB)</label>
          <select class="form-control @error('rpb') is-invalid @enderror" name="rpb">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->rpb == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->rpb == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->rpb == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->rpb == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->rpb == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->rpb == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->rpb == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->rpb }}>T</option>
          </select>
          @error('rpb')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Perancangan Interaksi (PI)</label>
          <select class="form-control @error('pi') is-invalid @enderror" name="pi">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->pi == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->pi == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->pi == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->pi == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->pi == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->pi == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->pi == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->pi }}>T</option>
          </select>
          @error('pi')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Analisis Perancangan Sistem Informasi (APSI)</label>
          <select class="form-control @error('apsi') is-invalid @enderror" name="apsi">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->apsi == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->apsi == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->apsi == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->apsi == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->apsi == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->apsi == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->apsi == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->apsi }}>T</option>
          </select>
          @error('apsi')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Web Application Development (WAD)</label>
          <select class="form-control @error('web') is-invalid @enderror" name="web">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->web == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->web == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->web == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->web == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->web == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->web == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->web == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->web }}>T</option>
          </select>
          @error('web')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Statistik</label>
          <select class="form-control @error('statistik') is-invalid @enderror" name="statistik">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->statistik == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->statistik == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->statistik == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->statistik == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->statistik == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->statistik == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->statistik == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->statistik }}>T</option>
          </select>
          @error('statistik')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Matematika Diskrit (Matdis)</label>
          <select class="form-control @error('matdis') is-invalid @enderror" name="matdis">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->matdis == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->matdis == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->matdis == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->matdis == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->matdis == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->matdis == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->matdis == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->matdis }}>T</option>
          </select>
          @error('matdis')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Algoritma dan Pemrograman (Alpro)</label>
          <select class="form-control @error('alpro') is-invalid @enderror" name="alpro">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->alpro == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->alpro == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->alpro == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->alpro == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->alpro == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->alpro == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->alpro == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->alpro }}>T</option>
          </select>
          @error('alpro')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Struktur Data (Strukdat)</label>
          <select class="form-control @error('strukdat') is-invalid @enderror" name="strukdat">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->strukdat == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->strukdat == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->strukdat == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->strukdat == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->strukdat == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->strukdat == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->strukdat == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->strukdat }}>T</option>
          </select>
          @error('strukdat')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>System Enterprise (SE)</label>
          <select class="form-control @error('se') is-invalid @enderror" name="se">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->se == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->se == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->se == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->se == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->se == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->se == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->se == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->se }}>T</option>
          </select>
          @error('se')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Perilaku Organisasi (PO)</label>
          <select class="form-control @error('po') is-invalid @enderror" name="po">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->po == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->po == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->po == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->po == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->po == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->po == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->po == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->po }}>T</option>
          </select>
          @error('po')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Supply Chain Management (SCM)</label>
          <select class="form-control @error('scm') is-invalid @enderror" name="scm">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->scm == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->scm == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->scm == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->scm == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->scm == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->scm == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->scm == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->scm }}>T</option>
          </select>
          @error('scm')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Enterprise Application (EA)</label>
          <select class="form-control @error('ea') is-invalid @enderror" name="ea">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->ea == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->ea == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->ea == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->ea == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->ea == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->ea == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->ea == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->ea }}>T</option>
          </select>
          @error('ea')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Basis Data (Basdat)</label>
          <select class="form-control @error('basdat') is-invalid @enderror" name="basdat">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->basdat == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->basdat == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->basdat == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->basdat == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->basdat == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->basdat == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->basdat == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->basdat }}>T</option>
          </select>
          @error('basdat')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Management Jaringan Komputer (Manjarkom)</label>
          <select class="form-control @error('manjarkom') is-invalid @enderror" name="manjarkom">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->manjarkom == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->manjarkom == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->manjarkom == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->manjarkom == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->manjarkom == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->manjarkom == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->manjarkom == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->manjarkom }}>T</option>
          </select>
          @error('manjarkom')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Sistem Operasi (Sisop)</label>
          <select class="form-control @error('sisop') is-invalid @enderror" name="sisop">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->sisop == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->sisop == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->sisop == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->sisop == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->sisop == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->sisop == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->sisop == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->sisop }}>T</option>
          </select>
          @error('sisop')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Management Sumber Daya Manusia (MSDM)</label>
          <select class="form-control @error('msdm') is-invalid @enderror" name="msdm">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->msdm == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->msdm == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->msdm == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->msdm == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->msdm == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->msdm == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->msdm == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->msdm }}>T</option>
          </select>
          @error('msdm')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Desain Jaringan (Desjar)</label>
          <select class="form-control @error('desjar') is-invalid @enderror" name="desjar">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->desjar == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->desjar == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->desjar == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->desjar == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->desjar == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->desjar == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->desjar == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->desjar }}>T</option>
          </select>
          @error('desjar')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Manajemen Proyek Sistem Informasi (Manprosi)</label>
          <select class="form-control @error('manprosi') is-invalid @enderror" name="manprosi">
            <option value="">Pilih Nilai</option>
            <option value="4" {{ $nilai->manprosi == '4' ? 'selected' : '' }}>A</option>
            <option value="3.5" {{ $nilai->manprosi == '3.5' ? 'selected' : '' }}>AB</option>
            <option value="3" {{ $nilai->manprosi == '3' ? 'selected' : '' }}>B</option>
            <option value="2.5" {{ $nilai->manprosi == '2.5' ? 'selected' : '' }}>BC</option>
            <option value="2" {{ $nilai->manprosi == '2' ? 'selected' : '' }}>C</option>
            <option value="1" {{ $nilai->manprosi == '1' ? 'selected' : '' }}>D</option>
            <option value="0" {{ $nilai->manprosi == '0' ? 'selected' : '' }}>E</option>
            <option value="0" {{ $nilai->manprosi }}>T</option>
          </select>
          @error('manprosi')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
