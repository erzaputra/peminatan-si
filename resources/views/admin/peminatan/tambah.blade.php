@extends('layouts.app-dashboard')

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Tambah Data Peminatan
    </div>
    <div class="card-body">
      <form action="{{ url('/master/data-peminatan/tambah/proses') }}" method="post">
        @csrf
        <div class="form-group">
          <label>Nama Peminatan</label>
          <input name="nama" type="text" class="form-control @error('nama') is-invalid @enderror" placeholder="Ex : Enterprise Data Management" value="{{ old('nama') }}">
          @error('nama')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Singkatan</label>
          <input name="singkatan" type="text" class="form-control @error('singkatan') is-invalid @enderror" placeholder="Ex : EDM" value="{{ old('singkatan') }}">
          @error('singkatan')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Kelompok Keahlian</label>
          <select class="form-control @error('kelompok') is-invalid @enderror" name="kelompok">
            <option value="">Pilih Kelompok</option>
            <option value="ESD" {{ old('kelompok') == 'ESD' ? 'selected' : '' }}>ESD</option>
            <option value="ESA" {{ old('kelompok') == 'ESA' ? 'selected' : '' }}>ESA</option>
          </select>
          @error('kelompok')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Keprofesian</label>
          <input name="keprof" type="text" class="form-control @error('keprof') is-invalid @enderror" placeholder="Ex : Daspro" value="{{ old('keprof') }}">
          @error('keprof')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
