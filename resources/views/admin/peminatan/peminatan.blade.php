@extends('layouts.app-dashboard')

@push('css')
<!-- Custom styles for this page -->
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Data Peminatan
      <a href="{{ url('/master/data-peminatan/tambah') }}" class="d-none d-sm-inline-block btn btn-sm btn-light shadow-sm float-right text-primary"><i class="fas fa-plus fa-sm text-primary"></i> Tambah Data</a>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Singkatan</th>
              <th>Kelompok</th>
              <th>Keprofesian</th>
              <th>Kuota</th>
              <th>Sisa Kuota</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($peminatan as $data)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $data->name }}</td>
              <td>{{ $data->singkatan }}</td>
              <td>{{ $data->kelompok }}</td>
              <td>{{ $data->keprof }}</td>
              <td>{{ $data->kuota == 0 ? "Belum Generate" : $data->kuota+$data->jumlah_prioritas }}</td>
              <td>{{ $data->kuota == 0 ? "Belum Generate" : $data->sisa_kuota }}</td>
              <td>
                <form class="" action="{{ url('/master/data-peminatan/delete/'.$data->id) }}" method="post">
                  @csrf
                  <button type="submit" name="delete" class="btn btn-danger" >Hapus</button>
                  <a href="{{ url('/master/data-peminatan/edit/'.$data->id) }}" class="btn btn-primary" >Ubah</a>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page level custom scripts -->
<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

<script type="text/javascript">
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endpush
