@extends('layouts.app-dashboard')

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Tambah Data Keprof
    </div>
    <div class="card-body">
      <form action="{{ url('/master/data-mahasiswa-keprof/tambah/proses') }}" method="post">
        @csrf
        <div class="form-group">
          <label>Identitas Mahasiswa</label>
          <select class="form-control @error('nim') is-invalid @enderror" name="nim">
            <option value="">Pilih Mahasiswa</option>
            @foreach($nilai as $data)
            <option value="{{ $data->nim }}" data-nama="{{ $data->nama }}" {{ old('nim') ==  $data->nim ? 'selected' : '' }}>{{ $data->nim.' - '.$data->nama }}</option>
            @endforeach
          </select>
          @error('nim')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Keprofesian</label>
          <select class="form-control @error('keprof') is-invalid @enderror" name="keprof">
            <option value="">Pilih Keprofesian</option>
            @foreach($peminatan as $data)
            <option value="{{ $data->id }}" {{ old('keprof') ==  $data->id ? 'selected' : '' }}>{{ $data->keprof }}</option>
            @endforeach
          </select>
          @error('keprof')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
