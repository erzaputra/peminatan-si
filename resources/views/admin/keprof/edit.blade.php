@extends('layouts.app-dashboard')

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Tambah Data Keprof
    </div>
    <div class="card-body">
      <form action="{{ url('/master/data-mahasiswa-keprof/edit/'.$mahasiswa[0]->id.'/proses') }}" method="post">
        @csrf
        <div class="form-group">
          <label>Identitas Mahasiswa</label>
          <input type="text" name="nim" value="{{ $mahasiswa[0]->nim }}" hidden>
          <input type="text" name="indentitas" value="{{ $mahasiswa[0]->nim.' - '.$mahasiswa[0]->data_nilai_mahasiswa->nama }}" class="form-control" disabled>
          @error('nim')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Keprofesian</label>
          <select class="form-control @error('keprof') is-invalid @enderror" name="keprof">
            <option value="">Pilih Keprofesian</option>
            @foreach($peminatan as $data)
            <option value="{{ $data->id }}" {{ $mahasiswa[0]->id_keprof ==  $data->id ? 'selected' : '' }}>{{ $data->keprof }}</option>
            @endforeach
          </select>
          @error('keprof')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
