@extends('layouts.app-dashboard')

@section('title', 'Hasil Seleksi')

@push('css')
<!-- Custom styles for this page -->
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white">
      Prediksi Kelulusan (CNN)
      <form class="d-inline" action="{{ url('/predictions/kelulusan-cnn/process') }}" method="post">
        @csrf
        <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm float-right text-white mr-3" data-toggle="modal" data-target="#inputModal">Generate Prediksi</button>
      </form>
      <form class="d-inline" action="{{ url('/predictions/kelulusan-cnn/reset') }}" method="post">
        @csrf
        <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm float-right text-white mr-3">Reset</button>
      </form>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No</th>
              <th>NIM</th>
              <th>Nama</th>
              <th>Peminatan</th>
              <th>Prediksi Kelulusan</th>
            </tr>
          </thead>
          <tbody>
            @foreach($hasil as $data)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $data->users->nim }}</td>
              <td>{{ $data->users->name }}</td>
              <td>
                  @if($data->peminatan != null)
                  {{ $data->peminatans->name }}
                  @else
                  Belum Masuk Peminatan
                  @endif
              </td>
              <td>
                <span class="font-weight-bold">
                  @if($data->status_lulus != null)
                  {{ $data->status_lulus == 0 ? 'Tidak Tepat Waktu' : 'Tepat Waktu' }}
                  @else
                  Belum Generate
                  @endif
                </span>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page level custom scripts -->
<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>
@endpush
