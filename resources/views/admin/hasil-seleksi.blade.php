@extends('layouts.app-dashboard')

@section('title', 'Hasil Seleksi')

@push('css')
<!-- Custom styles for this page -->
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      <form class="" action="{{ url('/hasil-seleksi/export') }}" method="post">
        @csrf
        Hasil Seleksi
        <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm float-right text-white mr-3" data-toggle="modal" data-target="#inputModal"><i class="fas fa-download fa-sm text-white"></i> Download Excel</button>
      </form>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No</th>
              <th>NIM</th>
              <th>Nama</th>
              <th>Pilihan 1</th>
              <th>Skor Pilihan 1</th>
              <th>Pilihan 2</th>
              <th>Skor Pilihan 2</th>
              <th>Peminatan</th>
            </tr>
          </thead>
          <tbody>
            @foreach($hasil as $data)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $data->users->nim }}</td>
              <td>{{ $data->users->name }}</td>
              <td>{{ $data->peminatan_1->name }}</td>
              <td>{{ $data->skor_peminatan_1 }}</td>
              <td>{{ $data->peminatan_2->name }}</td>
              <td>{{ $data->skor_peminatan_2 }}</td>
              <td>
                <span class="font-weight-bold">
                  @if($data->peminatan != null)
                  {{ $data->peminatans->name }}
                  @else
                  Belum Diterima
                  @endif
                </span>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Perbandingan Peminatan
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Peminatan</th>
              <th>Jumlah Pendaftar</th>
              <th>Jumlah Pilihan 1</th>
              <th>Jumlah Pilihan 2</th>
              <th>Pilihan 1</th>
              <th>Pilihan 2</th>
              <th>Bukan Pilihan</th>
              <th>Nilai Tertinggi</th>
              <th>Nilai Terendah</th>
              <th>Nilai Rata-rata</th>
            </tr>
          </thead>
          <tbody>
            @foreach($peminatan as $data)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $data->name }}</td>
              <td>{{ count($hasil->where('id_peminatan_1', $data->id)) + count($hasil->where('id_peminatan_2', $data->id)) }} </td>
              <td>{{ count($hasil->where('id_peminatan_1', $data->id)) }} </td>
              <td>{{ count($hasil->where('id_peminatan_2', $data->id)) }} </td>
              @if(count($hasil->where('peminatan', $data->id)) != 0)
              @if(count($hasil->where('id_peminatan_1', $data->id)) == 0)
              <td>0</td>
              @else
              <td>{{ count($hasil->where('peminatan', $data->id)->where('id_peminatan_1', $data->id))/count($hasil->where('id_peminatan_1', $data->id))*100 }}%</td>
              @endif
              @if(count($hasil->where('id_peminatan_2', $data->id)) == 0)
              <td>0</td>
              @else
              <td>{{ count($hasil->where('peminatan', $data->id)->where('id_peminatan_2', $data->id))/count($hasil->where('id_peminatan_2', $data->id))*100 }}%</td>
              @endif
              <td>{{ count($hasil->where('peminatan', $data->id)->where('skor_buang','!=', 0)) }}</td>
              <td>TBD</td>
              <td>TBD</td>
              <td>TBD</td>
              @else
              <td>Belum Diseleksi</td>
              <td>Belum Diseleksi</td>
              <td>Belum Diseleksi</td>
              <td>Belum Diseleksi</td>
              <td>Belum Diseleksi</td>
              <td>Belum Diseleksi</td>
              @endif
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Perbandingan Kelompok Keahlian
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Kelompok</th>
              <th>Jumlah Peminat</th>
              <th>Jumlah Diterima</th>
              <th>Rasio</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>ESD</td>
              <td>{{ count($hasil->where('id_peminatan_1', 1))+count($hasil->where('id_peminatan_1', 2))+count($hasil->where('id_peminatan_1', 3)) }}</td>
              <td>{{ count($hasil->where('peminatan', 1))+count($hasil->where('peminatan', 2))+count($hasil->where('peminatan', 3)) }}</td>
              @if(count($hasil->where('id_peminatan_1', 1)) == 0)
              <td>0</td>
              @else
              <td>{{ (count($hasil->where('peminatan', 1))+count($hasil->where('peminatan', 2))+count($hasil->where('peminatan', 3)))/(count($hasil->where('id_peminatan_1', 1))+count($hasil->where('id_peminatan_1', 2))+count($hasil->where('id_peminatan_1', 3)))*100 }}%</td>
              @endif
            </tr>
            <tr>
              <td>2</td>
              <td>ESA</td>
              <td>{{ count($hasil->where('id_peminatan_1', 4))+count($hasil->where('id_peminatan_1', 5))+count($hasil->where('id_peminatan_1', 6))+count($hasil->where('id_peminatan_1', 7)) }}</td>
              <td>{{ count($hasil->where('peminatan', 4))+count($hasil->where('peminatan', 5))+count($hasil->where('peminatan', 6))+count($hasil->where('peminatan', 7)) }}</td>
              @if(count($hasil->where('id_peminatan_1', 4)) == 0)
              <td>0</td>
              @else
              <td>{{ (count($hasil->where('peminatan', 4))+count($hasil->where('peminatan', 5))+count($hasil->where('peminatan', 6))+count($hasil->where('peminatan', 7)))/(count($hasil->where('id_peminatan_1', 4))+count($hasil->where('id_peminatan_1', 5))+count($hasil->where('id_peminatan_1', 6))+count($hasil->where('id_peminatan_1', 7)))*100 }}%</td>
              @endif
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page level custom scripts -->
<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>
@endpush