@extends('layouts.app-dashboard')

@push('css')
<!-- Custom styles for this page -->
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Mahasiswa Keprofesian
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No</th>
              <th>NIM</th>
              <th>Nama</th>
              <th>Pilihan 1</th>
              <th>Skor Pilihan 1</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($prioritas as $data)
            @if($data->prioritas != 1)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $data->users->nim }}</td>
              <td>{{ $data->users->name }}</td>
              <td>{{ $data->peminatan_1->name }}</td>
              <td>{{ $data->skor_peminatan_1 }}</td>
              <td>
                <form class="" action="{{ url('/prioritas/'.$data->id.'/proses') }}" method="post">
                  @csrf
                  <button type="submit" name="button" class="btn btn-primary">Pilih Prioritas</button>
                </form>
              </td>
            </tr>
            @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="card shadow mb-4">
    <div class="card-header bg-primary text-white py-3">
      Mahasiswa Prioritas
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No</th>
              <th>NIM</th>
              <th>Nama</th>
              <th>Pilihan 1</th>
              <th>Skor Pilihan 1</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($prioritas as $data)
            @if($data->prioritas == 1)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $data->users->nim }}</td>
              <td>{{ $data->users->name }}</td>
              <td>{{ $data->peminatan_1->name }}</td>
              <td>{{ $data->skor_peminatan_1 }}</td>
              <td>
                <form class="" action="{{ url('/prioritas/'.$data->id.'/cancel') }}" method="post">
                  @csrf
                  <button type="submit" name="button" class="btn btn-danger">Batal Prioritas</button>
                </form>
              </td>
            </tr>
            @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page level custom scripts -->
<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>
@endpush
