<table>
  <tr>
    <td>No</td>
    <td>NIM</td>
    <td>Nama</td>
    <td>Peminatan</td>
    <td>Mata Kuliah</td>
    <td>Kode Mata Kuliah</td>
    <td>Kelas Mata Kuliah</td>
    <td>ID</td>
  </tr>
  @foreach($hasil as $data)
  <tr>
    <td>{{ $loop->iteration }}</td>
    <td>{{ $data->users->nim }}</td>
    <td>{{ ucfirst($data->users->name) }}</td>
    @if($data->peminatan != null)
    <td>{{ $data->peminatans->name }}</td>
    <td>{{ $data->peminatans->matkul }}</td>
    <td>{{ $data->peminatans->kode_matkul }}</td>
    <td>SI-40-GAB1</td>
    @else
    <td>Belum Diseleksi</td>
    <td>Belum Diseleksi</td>
    <td>Belum Diseleksi</td>
    <td>Belum Diseleksi</td>
    @endif
  </tr>
  @endforeach
</table>
