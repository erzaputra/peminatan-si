<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Seleksi Peminatan | Credits</title>

  <!-- Custom fonts for this template-->
  <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>
<body>
  <div class="container-fluid">
    <div class="row p-5" style="height:100vh">
      <div class="col-md-12">
        <div class="my-5 justify-content-center align-self-center">
          <div class="text-center">
            <h1 class="font-weight-bold text-primary text-center">Pipe v1.0</h1>
            <h4 class="font-weight-bold text-primary text-center">(Pilih Peminatan)</h4>
            <hr class="w-50">
            <p>Pipe v1.0 dibuat dengan penuh drama, perasaan baper dan air mata programmer dua liter.</p>
            <p>Metode penyeleksian menggunakan <i>milea algorithm</i>.</p>
            <a href="https://www.instagram.com/erzaputra_/" target="_blank" class="text-white">- ERZ -</a>
          </div>
          <div class="mt-3 mb-0 text-center">
            <p>Copyright &copy 2020 &mdash; Sistem Informasi Tel-U</p>
          </div>
        </div>
      </div>
      <div class="col-md-12 text-center mb-5">
        <a href="{{ url('/home') }}">Back</a>
      </div>
    </div>
  </div>

  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
</body>
</html>
