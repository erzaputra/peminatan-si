<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  <!-- Sidebar - Brand -->
  <a class="sidebar-brand align-items-center text-center" href="{{ url('/') }}" style="height:200px;">
    <div class="sidebar-brand-icon mt-4 mb-2">
      <i class="fas fa-user"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Pilih</div>
    <div class="sidebar-brand-text mx-3">Peminatan</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  @if(Auth::user()->id_role == 2)

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="{{ url('/home') }}">
      <i class="fas fa-fw fa-home"></i>
      <span>Pilih Peminatan</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('/pengumuman') }}">
      <i class="fas fa-fw fa-file"></i>
      <span>Pengumuman</span>
    </a>
  </li>
  @elseif(Auth::user()->id_role == 3)

  <li class="nav-item active">
    <a class="nav-link" href="{{ url('/prioritas') }}">
      <i class="fas fa-fw fa-home"></i>
      <span>Prioritas Dashboard</span>
    </a>
  </li>
  @elseif(Auth::user()->id_role == 1)

  <li class="nav-item active">
    <a class="nav-link" href="{{ url('/home') }}">
      <i class="fas fa-fw fa-home"></i>
      <span>Panel Dashboard</span>
    </a>
  </li>
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Master Data
  </div>
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
      <i class="fas fa-fw fa-file"></i>
      <span>Master Data</span>
    </a>
    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="{{ url('/master/data-peminatan') }}">Data Peminatan</a>
        <a class="collapse-item" href="{{ url('/master/data-nilai-mahasiswa') }}">Data Nilai Mahasiswa</a>
        <a class="collapse-item" href="{{ url('/master/data-mahasiswa-keprof') }}">Data Mahasiswa Keprof</a>
        <a class="collapse-item" href="{{ url('/master/data-dosen') }}">Data Dosen</a>
      </div>
    </div>
  </li>
  <hr class="sidebar-divider">
  <div class="sidebar-heading">
    Seleksi
  </div>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('/hasil-seleksi') }}">
      <i class="fas fa-fw fa-file"></i>
      <span>Hasil Seleksi</span>
    </a>
  </li>
  <hr class="sidebar-divider">
  <div class="sidebar-heading">
    Prediksi
  </div>
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePrediction" aria-expanded="true" aria-controls="collapsePages">
      <i class="fas fa-fw fa-file"></i>
      <span>Predictions</span>
    </a>
    <div id="collapsePrediction" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="{{ url('/predictions/kelulusan-cnn') }}">Prediksi Kelulusan (CNN)</a>
      </div>
    </div>
  </li>
  <hr class="sidebar-divider">

  @endif

</ul>
<!-- End of Sidebar -->
