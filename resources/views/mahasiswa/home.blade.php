@extends('layouts.app-dashboard')

@section('title', 'Home')

@section('content')
<div class="container">
  <div class="col-md-12">
    <div class="card shadow mb-4">
      <div class="card-header bg-primary text-white">
        Pemilihan Peminatan
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-light shadow-sm float-right text-primary" data-toggle="modal" data-target="#inputModal"><i class="fas fa-plus fa-sm text-primary"></i> Tambah Peminatan</a>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Nama Mahasiswa</th>
                <th>Pilihan 1</th>
                <th>Pilihan 2</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @if($pilihan_peminatan != "[]")
              <tr>
                <td>{{ $pilihan_peminatan[0]->users->name }}</td>
                <td>{{ $pilihan_peminatan[0]->peminatan_1->name }} ({{ $pilihan_peminatan[0]->peminatan_1->singkatan }})</td>
                <td>{{ $pilihan_peminatan[0]->peminatan_2->name }} ({{ $pilihan_peminatan[0]->peminatan_2->singkatan }})</td>
                <td>
                  <form class="" action="{{ url('/data-peminatan/'.$pilihan_peminatan[0]->id.'/delete') }}" method="post">
                    @csrf
                    <button class="btn btn-danger" type="submit" name="button">Hapus</button>
                  </form>
                </td>
              </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="inputModal" tabindex="-1" role="dialog" aria-labelledby="inputModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-dark" id="exampleModalLabel">Input Pilihan Peminatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <small class="text-center mt-3">Pastikan NIM dan Nama anda benar</small>
      <form id="addinput" action="{{ url('/data-peminatan/tambah') }}" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="inputNim">NIM</label>
            <input value="{{ Auth::user()->nim }}" readonly type="integer" class="form-control" name="nim" id="nim">
          </div>
          <div class="form-group">
            <label for="inputNama">Nama Lengkap</label>
            <input value="{{ Auth::user()->name }}" readonly type="text" class="form-control" name="nama" id="nama">
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">Pilihan Peminatan 1</label>
            <select class="form-control" name="pilihan_1" id="pilihan_1">
              <option disabled value="" hidden selected>Pilih Salah Satu</option>
              @foreach($peminatan as $data)
              <option value="{{ $data->id }}" data-kelompok="{{ $data->kelompok }}">{{ $data->name }} ({{ $data->singkatan }})</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">Pilihan Peminatan 2</label>
            <select class="form-control" name="pilihan_2" id="pilihan_2">
              <option disabled value="" hidden selected>Pilih Salah Satu</option>
              @foreach($peminatan as $data)
              <option value="{{ $data->id }}" data-kelompok="{{ $data->kelompok }}" hidden>{{ $data->name }} ({{ $data->singkatan }})</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

@push('script')
<script type="text/javascript">
$('#pilihan_1').on('change', function(){
  $('#pilihan_2 option[data-kelompok]').attr('hidden', 'true');
  $('#pilihan_2').val("");
  var kelompok = $('#pilihan_1 option:selected').data('kelompok');
  if (kelompok == "ESD") {
    $('#pilihan_2 option[data-kelompok="ESA"]').removeAttr('hidden');
  }else{
    $('#pilihan_2 option[data-kelompok="ESD"]').removeAttr('hidden');
  }
});
</script>
@endpush
