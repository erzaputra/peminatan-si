@extends('layouts.app-dashboard')

@section('title', 'Pengumuman')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <div class="card p-5">
        <div class="card-body">
          <h5 class="text-primary">Selamat, <span class="font-weight-bold">{{ Auth::user()->name }}</span></h5>
          <p>Anda telah diterima di peminatan :</p>
          <h2 class="font-weight-bold text-primary my-4 ">{{ $peminatan->name }} ({{$peminatan->singkatan}})</h2>
          <p>Semangat kuliah, Semoga berhasil di perminatan tersebut. <span class="text-primary font-weight-bold">SHSD</span> !</p>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
