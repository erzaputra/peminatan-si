<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Seleksi Peminatan | Welcome</title>

  <!-- Custom fonts for this template-->
  <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>
<body>
  <div class="h-100 w-100" style="overflow-x:hidden;">
    <div class="row" style="height:100vh">
      <div class="col-xl-6 col-md-6 w-100">
        <img src="{{ asset('img/welcome.svg') }}" alt="welcome" height="100%" width="100%">
      </div>
      <div class="col-xl-6 col-md-6 bg-light">
        <div class="card border-0 p-3 bg-light d-flex">
          <div class="my-5 justify-content-center align-self-center">
            <div class="text-center">
              <h1 class="font-weight-bold text-primary text-center">Pipe</h1>
              <h4 class="font-weight-bold text-primary text-center">(Pilih Peminatan)</h4>
              <p>Seleksi peminatan lebih mudah dengan Pipe FRI.</p>
            </div>
            <div class="card-body">
              <div class="card col-md-12">
                <div class="card-header bg-white">
                  <h5 class="font-weight-bold text-primary mb-0 mt-3">Login</h5>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group row">
                      <label for="email" class="col-md-12 col-form-label text-md-left">Email</label>
                      <div class="col-md-12">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="password" class="col-md-12 col-form-label text-md-left">Password</label>
                      <div class="col-md-12">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                      </div>
                    </div>
                    <div class="form-group row mt-4">
                      <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary w-100">
                          Login
                        </button>
                      </div>
                    </div>
                    <div class="form-group row mb-3 mt-4">
                      <div class="col-md-12 text-center">
                        <a href="/register" class="btn btn-light text-primary w-100">
                          Daftar
                        </a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="mt-3 mb-0 text-center">
                <p>Copyright &copy 2020 &mdash; Sistem Informasi Tel-U</p>
              </div>
              <div class="text-center">
                <a href="/credit">Credits</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
  </body>
  </html>
