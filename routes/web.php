<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/credit', function(){
  return view('credit');
});
Route::get('/tidak-mendaftar', 'HomeController@tidakdaftar');

/*----dosen----*/
Route::middleware('dosen')->group(function () {
  Route::get('/prioritas', 'DataPeminatanController@prioritas');
  Route::post('/prioritas/{id}/proses', 'DataPeminatanController@proses_prioritas');
  Route::post('/prioritas/{id}/cancel', 'DataPeminatanController@cancel_prioritas');
});
/*----mahasiswa----*/
Route::middleware('mahasiswa')->group(function () {
  Route::post('/data-peminatan/tambah', 'DataPeminatanController@insert');
  Route::post('/data-peminatan/{id}/delete', 'DataPeminatanController@delete');
  Route::get('/pengumuman', 'HomeController@pengumuman');
});
/*----admin----*/
Route::middleware('admin')->group(function () {
  //peminatan
  Route::get('/master/data-peminatan', 'PeminatanController@index');
  Route::get('/master/data-peminatan/tambah', 'PeminatanController@tambah');
  Route::post('/master/data-peminatan/tambah/proses', 'PeminatanController@proses_tambah');
  Route::get('/master/data-peminatan/edit/{id}', 'PeminatanController@edit');
  Route::post('/master/data-peminatan/edit/{id}/proses', 'PeminatanController@proses_edit');
  Route::post('/master/data-peminatan/delete/{id}', 'PeminatanController@hapus');
  //nilai
  Route::get('/master/data-nilai-mahasiswa', 'NilaiController@index');
  Route::get('/master/data-nilai-mahasiswa/tambah', 'NilaiController@tambah');
  Route::post('/master/data-nilai-mahasiswa/tambah/proses', 'NilaiController@proses_tambah');
  Route::get('/master/data-nilai-mahasiswa/edit/{id}', 'NilaiController@edit');
  Route::post('/master/data-nilai-mahasiswa/edit/{id}/proses', 'NilaiController@proses_edit');
  Route::post('/master/data-nilai-mahasiswa/delete/{id}', 'NilaiController@hapus');
  Route::post('/master/data-nilai-mahasiswa/truncate', 'NilaiController@truncate');
  Route::post('/master/data-nilai-mahasiswa/import', 'NilaiController@import');
  //dosen
  Route::get('/master/data-dosen', 'DosenController@index');
  Route::get('/master/data-dosen/tambah', 'DosenController@tambah');
  Route::post('/master/data-dosen/tambah/proses', 'DosenController@proses_tambah');
  Route::get('/master/data-dosen/edit/{id}', 'DosenController@edit');
  Route::post('/master/data-dosen/edit/{id}/proses', 'DosenController@proses_edit');
  Route::post('/master/data-dosen/delete/{id}', 'DosenController@hapus');
  Route::post('/master/data-dosen/truncate', 'DosenController@truncate');
  Route::post('/master/data-dosen/import', 'DosenController@import');
  //keprof
  Route::get('/master/data-mahasiswa-keprof', 'KeprofesianController@index');
  Route::get('/master/data-mahasiswa-keprof/tambah', 'KeprofesianController@tambah');
  Route::post('/master/data-mahasiswa-keprof/tambah/proses', 'KeprofesianController@proses_tambah');
  Route::get('/master/data-mahasiswa-keprof/edit/{id}', 'KeprofesianController@edit');
  Route::post('/master/data-mahasiswa-keprof/edit/{id}/proses', 'KeprofesianController@proses_edit');
  Route::post('/master/data-mahasiswa-keprof/delete/{id}', 'KeprofesianController@hapus');
  Route::post('/master/data-mahasiswa-keprof/truncate', 'KeprofesianController@truncate');
  Route::post('/master/data-mahasiswa-keprof/import', 'KeprofesianController@import');

  //server
  Route::post('/server/{id}/status-update', 'ServerController@status');
  Route::post('/server/{id}/generate', 'ServerController@generate');
  //  Route::post('/server/{id}/generate', 'DataPeminatanController@seleksi');
  //hasil
  Route::get('/hasil-seleksi', 'DataPeminatanController@hasil');
  Route::post('/hasil-seleksi/export', 'DataPeminatanController@export');

  //predictions
  Route::get('/predictions/kelulusan-cnn', 'PredictionController@kelulusan_cnn_index');
  Route::post('/predictions/kelulusan-cnn/process', 'PredictionController@kelulusan_cnn_process');
  Route::post('/predictions/kelulusan-cnn/reset', 'PredictionController@kelulusan_cnn_reset');

});

// Route::get('/password-ganti', function(){
//   DB::table('users')->where('nim', 1234567)->update([
//     'password' => bcrypt('AdminPipeGanteng123'),
//   ]);
// });

// Route::get('/password-ganti', function(){
//   DB::table('users')->where('nim', 00000001)->update([
//     'password' => bcrypt('TechnoPipe123'),
//   ]);
//   DB::table('users')->where('nim', 00000002)->update([
//     'password' => bcrypt('EDMPipe123'),
//   ]);
//   DB::table('users')->where('nim', 00000003)->update([
//     'password' => bcrypt('ESDPipe123'),
//   ]);
//   DB::table('users')->where('nim', 00000004)->update([
//     'password' => bcrypt('ERPPipe123'),
//   ]);
//   DB::table('users')->where('nim', 00000005)->update([
//     'password' => bcrypt('EAPipe123'),
//   ]);
//   DB::table('users')->where('nim', 00000006)->update([
//     'password' => bcrypt('ISMPipe123'),
//   ]);
//   DB::table('users')->where('nim', 00000007)->update([
//     'password' => bcrypt('EIMPipe123'),
//   ]);
// });

// Route::get('/testing-controller', 'DataPeminatanController@seleksi');
